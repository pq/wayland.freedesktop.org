��P�      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �section���)��}�(hhh]�(h �title���)��}�(h�'Debugging touchpad pressure/size ranges�h]�h �Text����'Debugging touchpad pressure/size ranges�����}�(hh'hh%hhhNhNubah}�(h]�h]�h]�h]�h]�uhh#hh hhh�G/home/whot/code/libinput/build/doc/user/touchpad-pressure-debugging.rst�hKubh �	paragraph���)��}�(hX  :ref:`Touchpad pressure/size ranges <touchpad_pressure>` depend on
:ref:`device-quirks` entry specific to each laptop model. To check if a
pressure/size range is already defined for your device, use the
:ref:`libinput quirks <device-quirks-debugging>` tool: ::�h]�(�sphinx.addnodes��pending_xref���)��}�(h�8:ref:`Touchpad pressure/size ranges <touchpad_pressure>`�h]�h �inline���)��}�(hhAh]�h*�Touchpad pressure/size ranges�����}�(hhhhEubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhhChh?ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hP�refexplicit���	reftarget��touchpad_pressure��refdoc��touchpad-pressure-debugging��refwarn��uhh=hh5hKhh8ubh*� depend on
�����}�(h� depend on
�hh8hhhNhNubh>)��}�(h�:ref:`device-quirks`�h]�hD)��}�(hhkh]�h*�device-quirks�����}�(hhhhmubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChhiubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hw�refexplicit��h_�device-quirks�hahbhc�uhh=hh5hKhh8ubh*�t entry specific to each laptop model. To check if a
pressure/size range is already defined for your device, use the
�����}�(h�t entry specific to each laptop model. To check if a
pressure/size range is already defined for your device, use the
�hh8hhhNhNubh>)��}�(h�0:ref:`libinput quirks <device-quirks-debugging>`�h]�hD)��}�(hh�h]�h*�libinput quirks�����}�(hhhh�ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit��h_�device-quirks-debugging�hahbhc�uhh=hh5hKhh8ubh*� tool:�����}�(h� tool:�hh8hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hKhh hhubh �literal_block���)��}�(h�)$ libinput quirks list /dev/input/event19�h]�h*�)$ libinput quirks list /dev/input/event19�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhh�hKhh hhhh5ubh7)��}�(hX  If your device does not list any quirks, it probably needs a touch
pressure/size range, a palm threshold and a thumb threshold. Start with
:ref:`touchpad_pressure_hwdb`, then :ref:`touchpad_touch_size_hwdb`. The
respective tools will exit if the required axis is not supported.�h]�(h*��If your device does not list any quirks, it probably needs a touch
pressure/size range, a palm threshold and a thumb threshold. Start with
�����}�(h��If your device does not list any quirks, it probably needs a touch
pressure/size range, a palm threshold and a thumb threshold. Start with
�hh�hhhNhNubh>)��}�(h�:ref:`touchpad_pressure_hwdb`�h]�hD)��}�(hh�h]�h*�touchpad_pressure_hwdb�����}�(hhhh�ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hތrefexplicit��h_�touchpad_pressure_hwdb�hahbhc�uhh=hh5hKhh�ubh*�, then �����}�(h�, then �hh�hhhNhNubh>)��}�(h�:ref:`touchpad_touch_size_hwdb`�h]�hD)��}�(hh�h]�h*�touchpad_touch_size_hwdb�����}�(hhhh�ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j  �refexplicit��h_�touchpad_touch_size_hwdb�hahbhc�uhh=hh5hKhh�ubh*�G. The
respective tools will exit if the required axis is not supported.�����}�(h�G. The
respective tools will exit if the required axis is not supported.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hKhh hhubh �target���)��}�(h�.. _touchpad_pressure_hwdb:�h]�h}�(h]�h]�h]�h]�h]��refid��touchpad-pressure-hwdb�uhj  hKhh hhhh5ubh)��}�(hhh]�(h$)��}�(h�"Debugging touchpad pressure ranges�h]�h*�"Debugging touchpad pressure ranges�����}�(hj/  hj-  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh#hj*  hhhh5hKubh7)��}�(hX^  This section describes how to determine the touchpad pressure ranges
required for a touchpad device and how to add the required
:ref:`device-quirks` locally. Note that the quirk is **not public API** and **may
change at any time**. Users are advised to :ref:`report a bug <reporting_bugs>`
with the updated pressure ranges when testing has completed.�h]�(h*��This section describes how to determine the touchpad pressure ranges
required for a touchpad device and how to add the required
�����}�(h��This section describes how to determine the touchpad pressure ranges
required for a touchpad device and how to add the required
�hj;  hhhNhNubh>)��}�(h�:ref:`device-quirks`�h]�hD)��}�(hjF  h]�h*�device-quirks�����}�(hhhjH  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChjD  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�jR  �refexplicit��h_�device-quirks�hahbhc�uhh=hh5hKhj;  ubh*�! locally. Note that the quirk is �����}�(h�! locally. Note that the quirk is �hj;  hhhNhNubh �strong���)��}�(h�**not public API**�h]�h*�not public API�����}�(hhhji  ubah}�(h]�h]�h]�h]�h]�uhjg  hj;  ubh*� and �����}�(h� and �hj;  hhhNhNubjh  )��}�(h�**may
change at any time**�h]�h*�may
change at any time�����}�(hhhj|  ubah}�(h]�h]�h]�h]�h]�uhjg  hj;  ubh*�. Users are advised to �����}�(h�. Users are advised to �hj;  hhhNhNubh>)��}�(h�$:ref:`report a bug <reporting_bugs>`�h]�hD)��}�(hj�  h]�h*�report a bug�����}�(hhhj�  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h_�reporting_bugs�hahbhc�uhh=hh5hKhj;  ubh*�=
with the updated pressure ranges when testing has completed.�����}�(h�=
with the updated pressure ranges when testing has completed.�hj;  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hKhj*  hhubh7)��}�(h��Use the ``libinput measure touchpad-pressure`` tool provided by libinput.
This tool will search for your touchpad device and print some pressure
statistics, including whether a touch is/was considered logically down.�h]�(h*�Use the �����}�(h�Use the �hj�  hhhNhNubh �literal���)��}�(h�&``libinput measure touchpad-pressure``�h]�h*�"libinput measure touchpad-pressure�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh*�� tool provided by libinput.
This tool will search for your touchpad device and print some pressure
statistics, including whether a touch is/was considered logically down.�����}�(h�� tool provided by libinput.
This tool will search for your touchpad device and print some pressure
statistics, including whether a touch is/was considered logically down.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hKhj*  hhubh �note���)��}�(h�4This tool will only work on touchpads with pressure.�h]�h7)��}�(hj�  h]�h*�4This tool will only work on touchpads with pressure.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhh6hh5hK"hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj*  hhhh5hNubh7)��}�(h�'Example output of the tool is below: ::�h]�h*�$Example output of the tool is below:�����}�(h�$Example output of the tool is below:�hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh6hh5hK$hj*  hhubh�)��}�(hX{  $ sudo libinput measure touchpad-pressure
Ready for recording data.
Pressure range used: 8:10
Palm pressure range used: 65535
Place a single finger on the touchpad to measure pressure values.
Ctrl+C to exit
&nbsp;
Sequence 1190 pressure: min:  39 max:  48 avg:  43 median:  44 tags: down
Sequence 1191 pressure: min:  49 max:  65 avg:  62 median:  64 tags: down
Sequence 1192 pressure: min:  40 max:  78 avg:  64 median:  66 tags: down
Sequence 1193 pressure: min:  36 max:  83 avg:  70 median:  73 tags: down
Sequence 1194 pressure: min:  43 max:  76 avg:  72 median:  74 tags: down
Touchpad pressure:  47 min:  47 max:  86 tags: down�h]�h*X{  $ sudo libinput measure touchpad-pressure
Ready for recording data.
Pressure range used: 8:10
Palm pressure range used: 65535
Place a single finger on the touchpad to measure pressure values.
Ctrl+C to exit
&nbsp;
Sequence 1190 pressure: min:  39 max:  48 avg:  43 median:  44 tags: down
Sequence 1191 pressure: min:  49 max:  65 avg:  62 median:  64 tags: down
Sequence 1192 pressure: min:  40 max:  78 avg:  64 median:  66 tags: down
Sequence 1193 pressure: min:  36 max:  83 avg:  70 median:  73 tags: down
Sequence 1194 pressure: min:  43 max:  76 avg:  72 median:  74 tags: down
Touchpad pressure:  47 min:  47 max:  86 tags: down�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�h�h�uhh�hK+hj*  hhhh5ubh7)��}�(hX�  The example output shows five completed touch sequences and one ongoing one.
For each, the respective minimum and maximum pressure values are printed as
well as some statistics. The ``tags`` show that sequence was considered
logically down at some point. This is an interactive tool and its output may
change frequently. Refer to the <i>libinput-measure-touchpad-pressure(1)</i> man
page for more details.�h]�(h*��The example output shows five completed touch sequences and one ongoing one.
For each, the respective minimum and maximum pressure values are printed as
well as some statistics. The �����}�(h��The example output shows five completed touch sequences and one ongoing one.
For each, the respective minimum and maximum pressure values are printed as
well as some statistics. The �hj  hhhNhNubj�  )��}�(h�``tags``�h]�h*�tags�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubh*�� show that sequence was considered
logically down at some point. This is an interactive tool and its output may
change frequently. Refer to the <i>libinput-measure-touchpad-pressure(1)</i> man
page for more details.�����}�(h�� show that sequence was considered
logically down at some point. This is an interactive tool and its output may
change frequently. Refer to the <i>libinput-measure-touchpad-pressure(1)</i> man
page for more details.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hK5hj*  hhubh7)��}�(h��By default, this tool uses the :ref:`device-quirks` for the pressure range. To
narrow down on the best values for your device, specify the 'logically down'
and 'logically up' pressure thresholds with the  ``--touch-thresholds``
argument: ::�h]�(h*�By default, this tool uses the �����}�(h�By default, this tool uses the �hj4  hhhNhNubh>)��}�(h�:ref:`device-quirks`�h]�hD)��}�(hj?  h]�h*�device-quirks�����}�(hhhjA  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChj=  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�jK  �refexplicit��h_�device-quirks�hahbhc�uhh=hh5hK<hj4  ubh*�� for the pressure range. To
narrow down on the best values for your device, specify the ‘logically down’
and ‘logically up’ pressure thresholds with the  �����}�(h�� for the pressure range. To
narrow down on the best values for your device, specify the 'logically down'
and 'logically up' pressure thresholds with the  �hj4  hhhNhNubj�  )��}�(h�``--touch-thresholds``�h]�h*�--touch-thresholds�����}�(hhhj`  ubah}�(h]�h]�h]�h]�h]�uhj�  hj4  ubh*�

argument:�����}�(h�

argument:�hj4  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hK<hj*  hhubh�)��}�(h�U$ sudo libinput measure touchpad-pressure --touch-thresholds=10:8 --palm-threshold=20�h]�h*�U$ sudo libinput measure touchpad-pressure --touch-thresholds=10:8 --palm-threshold=20�����}�(hhhjy  ubah}�(h]�h]�h]�h]�h]�h�h�uhh�hKFhj*  hhhh5ubh7)��}�(h�ZInteract with the touchpad and check if the output of this tool matches your
expectations.�h]�h*�ZInteract with the touchpad and check if the output of this tool matches your
expectations.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh6hh5hKDhj*  hhubj�  )��}�(hX   This is an interactive process. You will need to re-run the
tool with varying thresholds until you find the right range for
your touchpad. Attaching output logs to a bug will not help, only
you with access to the hardware can figure out the correct
ranges.�h]�h7)��}�(hX   This is an interactive process. You will need to re-run the
tool with varying thresholds until you find the right range for
your touchpad. Attaching output logs to a bug will not help, only
you with access to the hardware can figure out the correct
ranges.�h]�h*X   This is an interactive process. You will need to re-run the
tool with varying thresholds until you find the right range for
your touchpad. Attaching output logs to a bug will not help, only
you with access to the hardware can figure out the correct
ranges.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhh6hh5hKGhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj*  hhhh5hNubh7)��}�(h�{Once the thresholds are decided on (e.g. 10 and 8), they can be enabled with
:ref:`device-quirks` entry similar to this: ::�h]�(h*�MOnce the thresholds are decided on (e.g. 10 and 8), they can be enabled with
�����}�(h�MOnce the thresholds are decided on (e.g. 10 and 8), they can be enabled with
�hj�  hhhNhNubh>)��}�(h�:ref:`device-quirks`�h]�hD)��}�(hj�  h]�h*�device-quirks�����}�(hhhj�  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h_�device-quirks�hahbhc�uhh=hh5hKMhj�  ubh*� entry similar to this:�����}�(h� entry similar to this:�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hKMhj*  hhubh�)��}�(h��$> cat /etc/libinput/local-overrides.quirks
[Touchpad pressure override]
MatchUdevType=touchpad
MatchName=*SynPS/2 Synaptics TouchPad
MatchDMIModalias=dmi:*svnLENOVO:*:pvrThinkPadX230*
AttrPressureRange=10:8�h]�h*��$> cat /etc/libinput/local-overrides.quirks
[Touchpad pressure override]
MatchUdevType=touchpad
MatchName=*SynPS/2 Synaptics TouchPad
MatchDMIModalias=dmi:*svnLENOVO:*:pvrThinkPadX230*
AttrPressureRange=10:8�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�h�h�uhh�hKUhj*  hhhh5ubh7)��}�(hX�  The file name **must** be ``/etc/libinput/local-overrides.quirks``. The
The first line is the section name and can be free-form. The ``Match``
directives limit the quirk to your touchpad, make sure the device name
matches your device's name (see ``libinput record``'s output). The dmi
modalias match should be based on the information in
``/sys/class/dmi/id/modalias``.  This modalias should be shortened to the
specific system's information, usually system vendor (svn)
and product name (pn).�h]�(h*�The file name �����}�(h�The file name �hj�  hhhNhNubjh  )��}�(h�**must**�h]�h*�must�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhjg  hj�  ubh*� be �����}�(h� be �hj�  hhhNhNubj�  )��}�(h�(``/etc/libinput/local-overrides.quirks``�h]�h*�$/etc/libinput/local-overrides.quirks�����}�(hhhj	  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh*�C. The
The first line is the section name and can be free-form. The �����}�(h�C. The
The first line is the section name and can be free-form. The �hj�  hhhNhNubj�  )��}�(h�	``Match``�h]�h*�Match�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh*�j
directives limit the quirk to your touchpad, make sure the device name
matches your device’s name (see �����}�(h�h
directives limit the quirk to your touchpad, make sure the device name
matches your device's name (see �hj�  hhhNhNubj�  )��}�(h�``libinput record``�h]�h*�libinput record�����}�(hhhj/  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh*�K’s output). The dmi
modalias match should be based on the information in
�����}�(h�I's output). The dmi
modalias match should be based on the information in
�hj�  hhhNhNubj�  )��}�(h�``/sys/class/dmi/id/modalias``�h]�h*�/sys/class/dmi/id/modalias�����}�(hhhjB  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh*�.  This modalias should be shortened to the
specific system’s information, usually system vendor (svn)
and product name (pn).�����}�(h�}.  This modalias should be shortened to the
specific system's information, usually system vendor (svn)
and product name (pn).�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hKWhj*  hhubh7)��}�(h�cOnce in place, run the following command to verify the quirk is valid and
works for your device: ::�h]�h*�`Once in place, run the following command to verify the quirk is valid and
works for your device:�����}�(h�`Once in place, run the following command to verify the quirk is valid and
works for your device:�hj[  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh6hh5hK`hj*  hhubh�)��}�(h�E$ sudo libinput list-quirks /dev/input/event10
AttrPressureRange=10:8�h]�h*�E$ sudo libinput list-quirks /dev/input/event10
AttrPressureRange=10:8�����}�(hhhjj  ubah}�(h]�h]�h]�h]�h]�h�h�uhh�hKhhj*  hhhh5ubh7)��}�(h��Replace the event node with the one from your device. If the
``AttrPressureRange`` quirk does not show up, re-run with ``--verbose`` and
check the output for any error messages.�h]�(h*�=Replace the event node with the one from your device. If the
�����}�(h�=Replace the event node with the one from your device. If the
�hjx  hhhNhNubj�  )��}�(h�``AttrPressureRange``�h]�h*�AttrPressureRange�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hjx  ubh*�% quirk does not show up, re-run with �����}�(h�% quirk does not show up, re-run with �hjx  hhhNhNubj�  )��}�(h�``--verbose``�h]�h*�	--verbose�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hjx  ubh*�- and
check the output for any error messages.�����}�(h�- and
check the output for any error messages.�hjx  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hKfhj*  hhubh7)��}�(h��If the pressure range quirk shows up correctly, restart X or the
Wayland compositor and libinput should now use the correct pressure
thresholds. The :ref:`tools` can be used to verify the correct
functionality first without the need for a restart.�h]�(h*��If the pressure range quirk shows up correctly, restart X or the
Wayland compositor and libinput should now use the correct pressure
thresholds. The �����}�(h��If the pressure range quirk shows up correctly, restart X or the
Wayland compositor and libinput should now use the correct pressure
thresholds. The �hj�  hhhNhNubh>)��}�(h�:ref:`tools`�h]�hD)��}�(hj�  h]�h*�tools�����}�(hhhj�  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h_�tools�hahbhc�uhh=hh5hKjhj�  ubh*�V can be used to verify the correct
functionality first without the need for a restart.�����}�(h�V can be used to verify the correct
functionality first without the need for a restart.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hKjhj*  hhubh7)��}�(h��Once the pressure ranges are deemed correct,
:ref:`report a bug <reporting_bugs>` to get the pressure ranges into the
repository.�h]�(h*�-Once the pressure ranges are deemed correct,
�����}�(h�-Once the pressure ranges are deemed correct,
�hj�  hhhNhNubh>)��}�(h�$:ref:`report a bug <reporting_bugs>`�h]�hD)��}�(hj�  h]�h*�report a bug�����}�(hhhj�  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h_�reporting_bugs�hahbhc�uhh=hh5hKohj�  ubh*�0 to get the pressure ranges into the
repository.�����}�(h�0 to get the pressure ranges into the
repository.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hKohj*  hhubj  )��}�(h�.. _touchpad_touch_size_hwdb:�h]�h}�(h]�h]�h]�h]�h]�j(  �touchpad-touch-size-hwdb�uhj  hKxhj*  hhhh5ubeh}�(h]�(�"debugging-touchpad-pressure-ranges�j)  eh]�h]�(�"debugging touchpad pressure ranges��touchpad_pressure_hwdb�eh]�h]�uhhhh hhhh5hK�expect_referenced_by_name�}�j"  j  s�expect_referenced_by_id�}�j)  j  subh)��}�(hhh]�(h$)��}�(h�Debugging touch size ranges�h]�h*�Debugging touch size ranges�����}�(hj.  hj,  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh#hj)  hhhh5hKwubh7)��}�(hXZ  This section describes how to determine the touchpad size ranges
required for a touchpad device and how to add the required
:ref:`device-quirks` locally. Note that the quirk is **not public API** and **may
change at any time**. Users are advised to :ref:`report a bug <reporting_bugs>`
with the updated pressure ranges when testing has completed.�h]�(h*�|This section describes how to determine the touchpad size ranges
required for a touchpad device and how to add the required
�����}�(h�|This section describes how to determine the touchpad size ranges
required for a touchpad device and how to add the required
�hj:  hhhNhNubh>)��}�(h�:ref:`device-quirks`�h]�hD)��}�(hjE  h]�h*�device-quirks�����}�(hhhjG  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChjC  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�jQ  �refexplicit��h_�device-quirks�hahbhc�uhh=hh5hKyhj:  ubh*�! locally. Note that the quirk is �����}�(h�! locally. Note that the quirk is �hj:  hhhNhNubjh  )��}�(h�**not public API**�h]�h*�not public API�����}�(hhhjf  ubah}�(h]�h]�h]�h]�h]�uhjg  hj:  ubh*� and �����}�(h� and �hj:  hhhNhNubjh  )��}�(h�**may
change at any time**�h]�h*�may
change at any time�����}�(hhhjy  ubah}�(h]�h]�h]�h]�h]�uhjg  hj:  ubh*�. Users are advised to �����}�(h�. Users are advised to �hj:  hhhNhNubh>)��}�(h�$:ref:`report a bug <reporting_bugs>`�h]�hD)��}�(hj�  h]�h*�report a bug�����}�(hhhj�  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h_�reporting_bugs�hahbhc�uhh=hh5hKyhj:  ubh*�=
with the updated pressure ranges when testing has completed.�����}�(h�=
with the updated pressure ranges when testing has completed.�hj:  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hKyhj)  hhubh7)��}�(h��Use the ``libinput measure touch-size`` tool provided by libinput.
This tool will search for your touchpad device and print some touch size
statistics, including whether a touch is/was considered logically down.�h]�(h*�Use the �����}�(h�Use the �hj�  hhhNhNubj�  )��}�(h�``libinput measure touch-size``�h]�h*�libinput measure touch-size�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh*�� tool provided by libinput.
This tool will search for your touchpad device and print some touch size
statistics, including whether a touch is/was considered logically down.�����}�(h�� tool provided by libinput.
This tool will search for your touchpad device and print some touch size
statistics, including whether a touch is/was considered logically down.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hKhj)  hhubj�  )��}�(h�EThis tool will only work on touchpads with the ``ABS_MT_MAJOR`` axis.�h]�h7)��}�(hj�  h]�(h*�/This tool will only work on touchpads with the �����}�(h�/This tool will only work on touchpads with the �hj�  ubj�  )��}�(h�``ABS_MT_MAJOR``�h]�h*�ABS_MT_MAJOR�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh*� axis.�����}�(h� axis.�hj�  ubeh}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj)  hhhh5hNubh7)��}�(h�'Example output of the tool is below: ::�h]�h*�$Example output of the tool is below:�����}�(h�$Example output of the tool is below:�hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj)  hhubh�)��}�(hX  $ sudo libinput measure touch-size --touch-thresholds 10:8 --palm-threshold 14
Using ELAN Touchscreen: /dev/input/event5
&nbsp;
Ready for recording data.
Touch sizes used: 10:8
Palm size used: 14
Place a single finger on the device to measure touch size.
Ctrl+C to exit
&nbsp;
Sequence: major: [  9.. 11] minor: [  7..  9]
Sequence: major: [  9.. 10] minor: [  7..  7]
Sequence: major: [  9.. 14] minor: [  6..  9]  down
Sequence: major: [ 11.. 11] minor: [  9..  9]  down
Sequence: major: [  4.. 33] minor: [  1..  5]  down palm�h]�h*X  $ sudo libinput measure touch-size --touch-thresholds 10:8 --palm-threshold 14
Using ELAN Touchscreen: /dev/input/event5
&nbsp;
Ready for recording data.
Touch sizes used: 10:8
Palm size used: 14
Place a single finger on the device to measure touch size.
Ctrl+C to exit
&nbsp;
Sequence: major: [  9.. 11] minor: [  7..  9]
Sequence: major: [  9.. 10] minor: [  7..  7]
Sequence: major: [  9.. 14] minor: [  6..  9]  down
Sequence: major: [ 11.. 11] minor: [  9..  9]  down
Sequence: major: [  4.. 33] minor: [  1..  5]  down palm�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�h�h�uhh�hK�hj)  hhhh5ubh7)��}�(hX�  The example output shows five completed touch sequences. For each, the
respective minimum and maximum pressure values are printed as well as some
statistics. The ``down`` and ``palm`` tags show that sequence was considered
logically down or a palm at some point. This is an interactive tool and its
output may change frequently. Refer to the <i>libinput-measure-touch-size(1)</i> man
page for more details.�h]�(h*��The example output shows five completed touch sequences. For each, the
respective minimum and maximum pressure values are printed as well as some
statistics. The �����}�(h��The example output shows five completed touch sequences. For each, the
respective minimum and maximum pressure values are printed as well as some
statistics. The �hj  hhhNhNubj�  )��}�(h�``down``�h]�h*�down�����}�(hhhj(  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubh*� and �����}�(h� and �hj  hhhNhNubj�  )��}�(h�``palm``�h]�h*�palm�����}�(hhhj;  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubh*�� tags show that sequence was considered
logically down or a palm at some point. This is an interactive tool and its
output may change frequently. Refer to the <i>libinput-measure-touch-size(1)</i> man
page for more details.�����}�(h�� tags show that sequence was considered
logically down or a palm at some point. This is an interactive tool and its
output may change frequently. Refer to the <i>libinput-measure-touch-size(1)</i> man
page for more details.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj)  hhubh7)��}�(hX  By default, this tool uses the :ref:`device-quirks` for the touch size range. To
narrow down on the best values for your device, specify the 'logically down'
and 'logically up' pressure thresholds with the  ``--touch-thresholds``
arguments as in the example above.�h]�(h*�By default, this tool uses the �����}�(h�By default, this tool uses the �hjT  hhhNhNubh>)��}�(h�:ref:`device-quirks`�h]�hD)��}�(hj_  h]�h*�device-quirks�����}�(hhhja  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChj]  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�jk  �refexplicit��h_�device-quirks�hahbhc�uhh=hh5hK�hjT  ubh*�� for the touch size range. To
narrow down on the best values for your device, specify the ‘logically down’
and ‘logically up’ pressure thresholds with the  �����}�(h�� for the touch size range. To
narrow down on the best values for your device, specify the 'logically down'
and 'logically up' pressure thresholds with the  �hjT  hhhNhNubj�  )��}�(h�``--touch-thresholds``�h]�h*�--touch-thresholds�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hjT  ubh*�#
arguments as in the example above.�����}�(h�#
arguments as in the example above.�hjT  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj)  hhubh7)��}�(h�ZInteract with the touchpad and check if the output of this tool matches your
expectations.�h]�h*�ZInteract with the touchpad and check if the output of this tool matches your
expectations.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj)  hhubj�  )��}�(hX   This is an interactive process. You will need to re-run the
tool with varying thresholds until you find the right range for
your touchpad. Attaching output logs to a bug will not help, only
you with access to the hardware can figure out the correct
ranges.�h]�h7)��}�(hX   This is an interactive process. You will need to re-run the
tool with varying thresholds until you find the right range for
your touchpad. Attaching output logs to a bug will not help, only
you with access to the hardware can figure out the correct
ranges.�h]�h*X   This is an interactive process. You will need to re-run the
tool with varying thresholds until you find the right range for
your touchpad. Attaching output logs to a bug will not help, only
you with access to the hardware can figure out the correct
ranges.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj)  hhhh5hNubh7)��}�(h�{Once the thresholds are decided on (e.g. 10 and 8), they can be enabled with
:ref:`device-quirks` entry similar to this: ::�h]�(h*�MOnce the thresholds are decided on (e.g. 10 and 8), they can be enabled with
�����}�(h�MOnce the thresholds are decided on (e.g. 10 and 8), they can be enabled with
�hj�  hhhNhNubh>)��}�(h�:ref:`device-quirks`�h]�hD)��}�(hj�  h]�h*�device-quirks�����}�(hhhj�  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h_�device-quirks�hahbhc�uhh=hh5hK�hj�  ubh*� entry similar to this:�����}�(h� entry similar to this:�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj)  hhubh�)��}�(h��$> cat /etc/libinput/local-overrides.quirks
[Touchpad touch size override]
MatchUdevType=touchpad
MatchName=*SynPS/2 Synaptics TouchPad
MatchDMIModalias=dmi:*svnLENOVO:*:pvrThinkPadX230*
AttrTouchSizeRange=10:8�h]�h*��$> cat /etc/libinput/local-overrides.quirks
[Touchpad touch size override]
MatchUdevType=touchpad
MatchName=*SynPS/2 Synaptics TouchPad
MatchDMIModalias=dmi:*svnLENOVO:*:pvrThinkPadX230*
AttrTouchSizeRange=10:8�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�h�h�uhh�hK�hj)  hhhh5ubh7)��}�(hXS  The first line is the match line and should be adjusted for the device name
(see :ref:`libinput record <libinput-record>`'s output) and for the local system, based on the
information in ``/sys/class/dmi/id/modalias``. The modalias should be
shortened to the specific system's information, usually system vendor (svn)
and product name (pn).�h]�(h*�QThe first line is the match line and should be adjusted for the device name
(see �����}�(h�QThe first line is the match line and should be adjusted for the device name
(see �hj�  hhhNhNubh>)��}�(h�(:ref:`libinput record <libinput-record>`�h]�hD)��}�(hj
  h]�h*�libinput record�����}�(hhhj  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChj  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j  �refexplicit��h_�libinput-record�hahbhc�uhh=hh5hK�hj�  ubh*�C’s output) and for the local system, based on the
information in �����}�(h�A's output) and for the local system, based on the
information in �hj�  hhhNhNubj�  )��}�(h�``/sys/class/dmi/id/modalias``�h]�h*�/sys/class/dmi/id/modalias�����}�(hhhj+  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh*�}. The modalias should be
shortened to the specific system’s information, usually system vendor (svn)
and product name (pn).�����}�(h�{. The modalias should be
shortened to the specific system's information, usually system vendor (svn)
and product name (pn).�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj)  hhubh7)��}�(h�cOnce in place, run the following command to verify the quirk is valid and
works for your device: ::�h]�h*�`Once in place, run the following command to verify the quirk is valid and
works for your device:�����}�(h�`Once in place, run the following command to verify the quirk is valid and
works for your device:�hjD  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj)  hhubh�)��}�(h�F$ sudo libinput list-quirks /dev/input/event10
AttrTouchSizeRange=10:8�h]�h*�F$ sudo libinput list-quirks /dev/input/event10
AttrTouchSizeRange=10:8�����}�(hhhjS  ubah}�(h]�h]�h]�h]�h]�h�h�uhh�hK�hj)  hhhh5ubh7)��}�(h��Replace the event node with the one from your device. If the
``AttrTouchSizeRange`` quirk does not show up, re-run with ``--verbose`` and
check the output for any error messages.�h]�(h*�=Replace the event node with the one from your device. If the
�����}�(h�=Replace the event node with the one from your device. If the
�hja  hhhNhNubj�  )��}�(h�``AttrTouchSizeRange``�h]�h*�AttrTouchSizeRange�����}�(hhhjj  ubah}�(h]�h]�h]�h]�h]�uhj�  hja  ubh*�% quirk does not show up, re-run with �����}�(h�% quirk does not show up, re-run with �hja  hhhNhNubj�  )��}�(h�``--verbose``�h]�h*�	--verbose�����}�(hhhj}  ubah}�(h]�h]�h]�h]�h]�uhj�  hja  ubh*�- and
check the output for any error messages.�����}�(h�- and
check the output for any error messages.�hja  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj)  hhubh7)��}�(h��If the touch size range property shows up correctly, restart X or the
Wayland compositor and libinput should now use the correct thresholds.
The :ref:`tools` can be used to verify the correct functionality first without
the need for a restart.�h]�(h*��If the touch size range property shows up correctly, restart X or the
Wayland compositor and libinput should now use the correct thresholds.
The �����}�(h��If the touch size range property shows up correctly, restart X or the
Wayland compositor and libinput should now use the correct thresholds.
The �hj�  hhhNhNubh>)��}�(h�:ref:`tools`�h]�hD)��}�(hj�  h]�h*�tools�����}�(hhhj�  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h_�tools�hahbhc�uhh=hh5hK�hj�  ubh*�V can be used to verify the correct functionality first without
the need for a restart.�����}�(h�V can be used to verify the correct functionality first without
the need for a restart.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj)  hhubh7)��}�(h�~Once the touch size ranges are deemed correct, :ref:`reporting_bugs` "report a
bug" to get the thresholds into the repository.�h]�(h*�/Once the touch size ranges are deemed correct, �����}�(h�/Once the touch size ranges are deemed correct, �hj�  hhhNhNubh>)��}�(h�:ref:`reporting_bugs`�h]�hD)��}�(hj�  h]�h*�reporting_bugs�����}�(hhhj�  ubah}�(h]�h]�(hO�std��std-ref�eh]�h]�h]�uhhChj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h_�reporting_bugs�hahbhc�uhh=hh5hK�hj�  ubh*�> “report a
bug” to get the thresholds into the repository.�����}�(h�: "report a
bug" to get the thresholds into the repository.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh6hh5hK�hj)  hhubeh}�(h]�(�debugging-touch-size-ranges�j  eh]�h]�(�debugging touch size ranges��touchpad_touch_size_hwdb�eh]�h]�uhhhh hhhh5hKwj%  }�j   j  sj'  }�j  j  subeh}�(h]��'debugging-touchpad-pressure-size-ranges�ah]�h]��'debugging touchpad pressure/size ranges�ah]�h]�uhhhhhhhh5hKubeh}�(h]�h]�h]�h]�h]��source�h5uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h#N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j0  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h5�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`424e27f`�h]�h �	reference���)��}�(h�git commit 424e27f�h]�h*�git commit 424e27f�����}�(hhhjp  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/424e27f�uhjn  hjj  ubah}�(h]�h]�h]�jg  ah]�h]�uhjh  h�<rst_prolog>�hKhhub�git_version_full�ji  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f5e21ad5f28>`

�h]�jo  )��}�(h�<git commit <function get_git_version_full at 0x7f5e21ad5f28>�h]�h*�<git commit <function get_git_version_full at 0x7f5e21ad5f28>�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f5e21ad5f28>�uhjn  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhjh  hj�  hKhhubu�substitution_names�}�(�git_version�jg  �git_version_full�j�  u�refnames�}��refids�}�(j)  ]�j  aj  ]�j  au�nameids�}�(j
  j  j"  j)  j!  j  j   j  j�  j�  u�	nametypes�}�(j
  Nj"  �j!  Nj   �j�  Nuh}�(j  h j)  j*  j  j*  j  j)  j�  j)  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h7)��}�(hhh]�h*�<Hyperlink target "touchpad-pressure-hwdb" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh6hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h5�line�Kuhj�  ubj�  )��}�(hhh]�h7)��}�(hhh]�h*�>Hyperlink target "touchpad-touch-size-hwdb" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh6hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h5�line�Kxuhj�  ube�transformer�N�
decoration�Nhhub.