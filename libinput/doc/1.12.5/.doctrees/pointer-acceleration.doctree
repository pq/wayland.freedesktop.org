���      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`7d25d9e`�h]�h �	reference���)��}�(h�git commit 7d25d9e�h]�h �Text����git commit 7d25d9e�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/7d25d9e�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4b9dfe32f0>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�h]�h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4b9dfe32f0>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _pointer-acceleration:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��pointer-acceleration�uh0h]h:Kh hhhh8�@/home/whot/code/libinput/build/doc/user/pointer-acceleration.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Pointer acceleration�h]�h�Pointer acceleration�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h��libinput uses device-specific pointer acceleration methods, with the default
being the :ref:`ptraccel-linear`. The methods share common properties, such as
:ref:`ptraccel-velocity`.�h]�(h�Wlibinput uses device-specific pointer acceleration methods, with the default
being the �����}�(h�Wlibinput uses device-specific pointer acceleration methods, with the default
being the �h h�hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`ptraccel-linear`�h]�h �inline���)��}�(hh�h]�h�ptraccel-linear�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h��refexplicit���	reftarget��ptraccel-linear��refdoc��pointer-acceleration��refwarn��uh0h�h8hkh:Kh h�ubh�/. The methods share common properties, such as
�����}�(h�/. The methods share common properties, such as
�h h�hhh8Nh:Nubh�)��}�(h�:ref:`ptraccel-velocity`�h]�h�)��}�(hh�h]�h�ptraccel-velocity�����}�(hhh h�ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�hǌrefexplicit��h��ptraccel-velocity�h�h�h��uh0h�h8hkh:Kh h�ubh�.�����}�(h�.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h��This page explains the high-level concepts used in the code. It aims to
provide an overview for developers and is not necessarily useful for
users.�h]�h��This page explains the high-level concepts used in the code. It aims to
provide an overview for developers and is not necessarily useful for
users.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�.. _ptraccel-profiles:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�ptraccel-profiles�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�Pointer acceleration profiles�h]�h�Pointer acceleration profiles�����}�(hj   h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh h�hhh8hkh:Kubh�)��}�(hX�  The profile decides the general method of pointer acceleration.
libinput currently supports two profiles: "adaptive" and "flat". The adaptive
profile is the default profile for all devices and takes the current speed
of the device into account when deciding on acceleration. The flat profile
is simply a constant factor applied to all device deltas, regardless of the
speed of motion (see :ref:`ptraccel-profile-flat`). Most of this document
describes the adaptive pointer acceleration.�h]�(hX�  The profile decides the general method of pointer acceleration.
libinput currently supports two profiles: “adaptive” and “flat”. The adaptive
profile is the default profile for all devices and takes the current speed
of the device into account when deciding on acceleration. The flat profile
is simply a constant factor applied to all device deltas, regardless of the
speed of motion (see �����}�(hX�  The profile decides the general method of pointer acceleration.
libinput currently supports two profiles: "adaptive" and "flat". The adaptive
profile is the default profile for all devices and takes the current speed
of the device into account when deciding on acceleration. The flat profile
is simply a constant factor applied to all device deltas, regardless of the
speed of motion (see �h j  hhh8Nh:Nubh�)��}�(h�:ref:`ptraccel-profile-flat`�h]�h�)��}�(hj  h]�h�ptraccel-profile-flat�����}�(hhh j  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j#  �refexplicit��h��ptraccel-profile-flat�h�h�h��uh0h�h8hkh:Kh j  ubh�E). Most of this document
describes the adaptive pointer acceleration.�����}�(h�E). Most of this document
describes the adaptive pointer acceleration.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh^)��}�(h�.. _ptraccel-velocity:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�ptraccel-velocity�uh0h]h:K"h h�hhh8hkubeh!}�(h#]�(�pointer-acceleration-profiles�h�eh%]�h']�(�pointer acceleration profiles��ptraccel-profiles�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�jO  h�s�expect_referenced_by_id�}�h�h�subhm)��}�(hhh]�(hr)��}�(h�Velocity calculation�h]�h�Velocity calculation�����}�(hj[  h jY  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jV  hhh8hkh:K!ubh�)��}�(hX  The device's speed of movement is measured across multiple input events
through so-called "trackers". Each event prepends a the tracker item, each
subsequent tracker contains the delta of that item to the current position,
the timestamp of the event that created it and the cardinal direction of the
movement at the time. If a device moves into the same direction, the
velocity is calculated across multiple trackers. For example, if a device
moves steadily for 10 events to the left, the velocity is calculated across
all 10 events.�h]�hX  The device’s speed of movement is measured across multiple input events
through so-called “trackers”. Each event prepends a the tracker item, each
subsequent tracker contains the delta of that item to the current position,
the timestamp of the event that created it and the cardinal direction of the
movement at the time. If a device moves into the same direction, the
velocity is calculated across multiple trackers. For example, if a device
moves steadily for 10 events to the left, the velocity is calculated across
all 10 events.�����}�(hji  h jg  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K#h jV  hhubh�)��}�(hX  Whenever the movement changes direction or significantly changes speed, the
velocity is calculated from the direction/speed change only. For example, if
a device moves steadily for 8 events to the left and then 2 events to the
right, the velocity is only that of the last 2 events.�h]�hX  Whenever the movement changes direction or significantly changes speed, the
velocity is calculated from the direction/speed change only. For example, if
a device moves steadily for 8 events to the left and then 2 events to the
right, the velocity is only that of the last 2 events.�����}�(hjw  h ju  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K,h jV  hhubh�)��}�(hX  An extra time limit prevents events that are too old to factor into the
velocity calculation. For example, if a device moves steadily for 5 events
to the left, then pauses, then moves again for 5 events to the left, only
the last 5 events are used for velocity calculation.�h]�hX  An extra time limit prevents events that are too old to factor into the
velocity calculation. For example, if a device moves steadily for 5 events
to the left, then pauses, then moves again for 5 events to the left, only
the last 5 events are used for velocity calculation.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K1h jV  hhubh�)��}�(h�>The velocity is then used to calculate the acceleration factor�h]�h�>The velocity is then used to calculate the acceleration factor�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K6h jV  hhubh^)��}�(h�.. _ptraccel-factor:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�ptraccel-factor�uh0h]h:K=h jV  hhh8hkubeh!}�(h#]�(�velocity-calculation�jH  eh%]�h']�(�velocity calculation��ptraccel-velocity�eh)]�h+]�uh0hlh hnhhh8hkh:K!jR  }�j�  j>  sjT  }�jH  j>  subhm)��}�(hhh]�(hr)��}�(h�Acceleration factor�h]�h�Acceleration factor�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K<ubh�)��}�(hX  The acceleration factor is the final outcome of the pointer acceleration
calculations. It is a unitless factor that is applied to the current delta,
a factor of 2 doubles the delta (i.e. speeds up the movement), a factor of
less than 1 reduces the delta (i.e. slows the movement).�h]�hX  The acceleration factor is the final outcome of the pointer acceleration
calculations. It is a unitless factor that is applied to the current delta,
a factor of 2 doubles the delta (i.e. speeds up the movement), a factor of
less than 1 reduces the delta (i.e. slows the movement).�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K>h j�  hhubh�)��}�(hX(  Any factor less than 1 requires the user to move the device further to move
the visible pointer. This is called deceleration and enables high precision
target selection through subpixel movements. libinput's current maximum
deceleration factor is 0.3 (i.e. slow down to 30% of the pointer speed).�h]�hX*  Any factor less than 1 requires the user to move the device further to move
the visible pointer. This is called deceleration and enables high precision
target selection through subpixel movements. libinput’s current maximum
deceleration factor is 0.3 (i.e. slow down to 30% of the pointer speed).�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KCh j�  hhubh�)��}�(h��A factor higher than 1 moves the pointer further than the physical device
moves. This is acceleration and allows a user to cross the screen quickly
but effectively skips pixels. libinput's current maximum acceleration factor
is 3.5.�h]�h��A factor higher than 1 moves the pointer further than the physical device
moves. This is acceleration and allows a user to cross the screen quickly
but effectively skips pixels. libinput’s current maximum acceleration factor
is 3.5.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KHh j�  hhubh^)��}�(h�.. _ptraccel-linear:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�ptraccel-linear�uh0h]h:KRh j�  hhh8hkubeh!}�(h#]�(�acceleration-factor�j�  eh%]�h']�(�acceleration factor��ptraccel-factor�eh)]�h+]�uh0hlh hnhhh8hkh:K<jR  }�j  j�  sjT  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Linear pointer acceleration�h]�h�Linear pointer acceleration�����}�(hj  h j	  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j  hhh8hkh:KQubh�)��}�(h��The linear pointer acceleration method is the default for most pointer
devices. It provides deceleration at very slow movements, a 1:1 mapping for
regular movements and a linear increase to the maximum acceleration factor
for fast movements.�h]�h��The linear pointer acceleration method is the default for most pointer
devices. It provides deceleration at very slow movements, a 1:1 mapping for
regular movements and a linear increase to the maximum acceleration factor
for fast movements.�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KSh j  hhubh�)��}�(h�~Linear pointer acceleration applies to devices with above 1000dpi resolution
and after :ref:`motion_normalization` is applied.�h]�(h�WLinear pointer acceleration applies to devices with above 1000dpi resolution
and after �����}�(h�WLinear pointer acceleration applies to devices with above 1000dpi resolution
and after �h j%  hhh8Nh:Nubh�)��}�(h�:ref:`motion_normalization`�h]�h�)��}�(hj0  h]�h�motion_normalization�����}�(hhh j2  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j.  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j<  �refexplicit��h��motion_normalization�h�h�h��uh0h�h8hkh:KXh j%  ubh� is applied.�����}�(h� is applied.�h j%  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KXh j  hhubh �figure���)��}�(hhh]�(h �image���)��}�(h�T.. figure:: ptraccel-linear.svg
    :align: center

    Linear pointer acceleration
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��ptraccel-linear.svg��
candidates�}��*�ji  suh0j\  h jY  h8hkh:K^ubh �caption���)��}�(h�Linear pointer acceleration�h]�h�Linear pointer acceleration�����}�(hjq  h jo  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jm  h8hkh:K^h jY  ubeh!}�(h#]��id2�ah%]�h']�h)]�h+]��align��center�uh0jW  h:K^h j  hhh8hkubh�)��}�(hXi  The image above shows the linear pointer acceleration settings at various
speeds. The line for 0.0 is the default acceleration curve, speed settings
above 0.0 accelerate sooner, faster and to a higher maximum acceleration.
Speed settings below 0 delay when acceleration kicks in, how soon the
maximum acceleration is reached and the maximum acceleration factor.�h]�hXi  The image above shows the linear pointer acceleration settings at various
speeds. The line for 0.0 is the default acceleration curve, speed settings
above 0.0 accelerate sooner, faster and to a higher maximum acceleration.
Speed settings below 0 delay when acceleration kicks in, how soon the
maximum acceleration is reached and the maximum acceleration factor.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K`h j  hhubh�)��}�(h�sExtremely low speed settings provide no acceleration and additionally
decelerate all movement by a constant factor.�h]�h�sExtremely low speed settings provide no acceleration and additionally
decelerate all movement by a constant factor.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kfh j  hhubh^)��}�(h�.. _ptraccel-low-dpi:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�ptraccel-low-dpi�uh0h]h:Knh j  hhh8hkubeh!}�(h#]�(�linear-pointer-acceleration�j�  eh%]�h']�(�linear pointer acceleration��ptraccel-linear�eh)]�h+]�uh0hlh hnhhh8hkh:KQjR  }�j�  j�  sjT  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�(Pointer acceleration for low-dpi devices�h]�h�(Pointer acceleration for low-dpi devices�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:Kmubh�)��}�(hXP  Low-dpi devices are those with a physical resolution of less than 1000 dots
per inch (dpi). The pointer acceleration is adjusted to provide roughly the
same feel for all devices at normal to high speeds. At slow speeds, the
pointer acceleration works on device-units rather than normalized
coordinates (see :ref:`motion_normalization`).�h]�(hX3  Low-dpi devices are those with a physical resolution of less than 1000 dots
per inch (dpi). The pointer acceleration is adjusted to provide roughly the
same feel for all devices at normal to high speeds. At slow speeds, the
pointer acceleration works on device-units rather than normalized
coordinates (see �����}�(hX3  Low-dpi devices are those with a physical resolution of less than 1000 dots
per inch (dpi). The pointer acceleration is adjusted to provide roughly the
same feel for all devices at normal to high speeds. At slow speeds, the
pointer acceleration works on device-units rather than normalized
coordinates (see �h j�  hhh8Nh:Nubh�)��}�(h�:ref:`motion_normalization`�h]�h�)��}�(hj�  h]�h�motion_normalization�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h��motion_normalization�h�h�h��uh0h�h8hkh:Koh j�  ubh�).�����}�(h�).�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Koh j�  hhubjX  )��}�(hhh]�(j]  )��}�(h�b.. figure:: ptraccel-low-dpi.svg
    :align: center

    Pointer acceleration for low-dpi devices
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��ptraccel-low-dpi.svg�jj  }�jl  j	  suh0j\  h j�  h8hkh:Kxubjn  )��}�(h�(Pointer acceleration for low-dpi devices�h]�h�(Pointer acceleration for low-dpi devices�����}�(hj  h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jm  h8hkh:Kxh j�  ubeh!}�(h#]��id3�ah%]�h']�h)]�h+]�j�  �center�uh0jW  h:Kxh j�  hhh8hkubh�)��}�(h��The image above shows the default pointer acceleration curve for a speed of
0.0 at different DPI settings. A device with low DPI has the acceleration
applied sooner and with a stronger acceleration factor.�h]�h��The image above shows the default pointer acceleration curve for a speed of
0.0 at different DPI settings. A device with low DPI has the acceleration
applied sooner and with a stronger acceleration factor.�����}�(hj#  h j!  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kzh j�  hhubh^)��}�(h�.. _ptraccel-touchpad:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�ptraccel-touchpad�uh0h]h:K�h j�  hhh8hkubeh!}�(h#]�(�(pointer-acceleration-for-low-dpi-devices�j�  eh%]�h']�(�(pointer acceleration for low-dpi devices��ptraccel-low-dpi�eh)]�h+]�uh0hlh hnhhh8hkh:KmjR  }�j@  j�  sjT  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�!Pointer acceleration on touchpads�h]�h�!Pointer acceleration on touchpads�����}�(hjJ  h jH  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jE  hhh8hkh:K�ubh�)��}�(hX.  Touchpad pointer acceleration uses the same approach as the
:ref:`ptraccel-linear` profile, with a constant deceleration factor applied. The
user expectation of how much a pointer should move in response to finger
movement is different to that of a mouse device, hence the constant
deceleration factor.�h]�(h�<Touchpad pointer acceleration uses the same approach as the
�����}�(h�<Touchpad pointer acceleration uses the same approach as the
�h jV  hhh8Nh:Nubh�)��}�(h�:ref:`ptraccel-linear`�h]�h�)��}�(hja  h]�h�ptraccel-linear�����}�(hhh jc  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j_  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jm  �refexplicit��h��ptraccel-linear�h�h�h��uh0h�h8hkh:K�h jV  ubh�� profile, with a constant deceleration factor applied. The
user expectation of how much a pointer should move in response to finger
movement is different to that of a mouse device, hence the constant
deceleration factor.�����}�(h�� profile, with a constant deceleration factor applied. The
user expectation of how much a pointer should move in response to finger
movement is different to that of a mouse device, hence the constant
deceleration factor.�h jV  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h jE  hhubjX  )��}�(hhh]�(j]  )��}�(h�c.. figure:: ptraccel-touchpad.svg
    :align: center

    Pointer acceleration curve for touchpads
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��ptraccel-touchpad.svg�jj  }�jl  j�  suh0j\  h j�  h8hkh:K�ubjn  )��}�(h�(Pointer acceleration curve for touchpads�h]�h�(Pointer acceleration curve for touchpads�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jm  h8hkh:K�h j�  ubeh!}�(h#]��id4�ah%]�h']�h)]�h+]�j�  �center�uh0jW  h:K�h jE  hhh8hkubh�)��}�(h��The image above shows the touchpad acceleration profile in comparison to the
:ref:`ptraccel-linear`. The shape of the curve is identical but vertically squashed.�h]�(h�MThe image above shows the touchpad acceleration profile in comparison to the
�����}�(h�MThe image above shows the touchpad acceleration profile in comparison to the
�h j�  hhh8Nh:Nubh�)��}�(h�:ref:`ptraccel-linear`�h]�h�)��}�(hj�  h]�h�ptraccel-linear�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h��ptraccel-linear�h�h�h��uh0h�h8hkh:K�h j�  ubh�>. The shape of the curve is identical but vertically squashed.�����}�(h�>. The shape of the curve is identical but vertically squashed.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h jE  hhubh^)��}�(h�.. _ptraccel-trackpoint:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�ptraccel-trackpoint�uh0h]h:K�h jE  hhh8hkubeh!}�(h#]�(�!pointer-acceleration-on-touchpads�j9  eh%]�h']�(�!pointer acceleration on touchpads��ptraccel-touchpad�eh)]�h+]�uh0hlh hnhhh8hkh:K�jR  }�j�  j/  sjT  }�j9  j/  subhm)��}�(hhh]�(hr)��}�(h�#Pointer acceleration on trackpoints�h]�h�#Pointer acceleration on trackpoints�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K�ubh�)��}�(hX�  The main difference between trackpoint hardware and mice or touchpads is
that trackpoint speed is a function of pressure rather than moving speed.
But trackpoint hardware is quite varied in how it reacts to user pressure
and unlike other devices it cannot easily be normalized for physical
properties. Measuring pressure objectively across a variety of hardware is
nontrivial. See :ref:`trackpoints` for more details.�h]�(hX}  The main difference between trackpoint hardware and mice or touchpads is
that trackpoint speed is a function of pressure rather than moving speed.
But trackpoint hardware is quite varied in how it reacts to user pressure
and unlike other devices it cannot easily be normalized for physical
properties. Measuring pressure objectively across a variety of hardware is
nontrivial. See �����}�(hX}  The main difference between trackpoint hardware and mice or touchpads is
that trackpoint speed is a function of pressure rather than moving speed.
But trackpoint hardware is quite varied in how it reacts to user pressure
and unlike other devices it cannot easily be normalized for physical
properties. Measuring pressure objectively across a variety of hardware is
nontrivial. See �h j  hhh8Nh:Nubh�)��}�(h�:ref:`trackpoints`�h]�h�)��}�(hj  h]�h�trackpoints�����}�(hhh j  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j  �refexplicit��h��trackpoints�h�h�h��uh0h�h8hkh:K�h j  ubh� for more details.�����}�(h� for more details.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh�)��}�(hX�  The deltas for trackpoints are converted units/ms but there is no common
physical reference point for a unit. Thus, the same pressure on different
trackpoints will generate different speeds and thus different acceleration
behaviors. Additionally, some trackpoints provide the ability to adjust the
sensitivity in hardware by modifying a sysfs file on the serio node. A
higher sensitivity results in higher deltas, thus changing the definition of
what is a unit again.�h]�hX�  The deltas for trackpoints are converted units/ms but there is no common
physical reference point for a unit. Thus, the same pressure on different
trackpoints will generate different speeds and thus different acceleration
behaviors. Additionally, some trackpoints provide the ability to adjust the
sensitivity in hardware by modifying a sysfs file on the serio node. A
higher sensitivity results in higher deltas, thus changing the definition of
what is a unit again.�����}�(hj;  h j9  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh�)��}�(h��libinput attempts to normalize unit data to the best of its abilities, see
:ref:`trackpoint_multiplier`. Beyond this, it is not possible to have
consistent behavior across different touchpad devices.�h]�(h�Klibinput attempts to normalize unit data to the best of its abilities, see
�����}�(h�Klibinput attempts to normalize unit data to the best of its abilities, see
�h jG  hhh8Nh:Nubh�)��}�(h�:ref:`trackpoint_multiplier`�h]�h�)��}�(hjR  h]�h�trackpoint_multiplier�����}�(hhh jT  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h jP  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j^  �refexplicit��h��trackpoint_multiplier�h�h�h��uh0h�h8hkh:K�h jG  ubh�`. Beyond this, it is not possible to have
consistent behavior across different touchpad devices.�����}�(h�`. Beyond this, it is not possible to have
consistent behavior across different touchpad devices.�h jG  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubjX  )��}�(hhh]�(j]  )��}�(h�h.. figure:: ptraccel-trackpoint.svg
    :align: center

    Pointer acceleration curves for trackpoints
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��ptraccel-trackpoint.svg�jj  }�jl  j�  suh0j\  h jy  h8hkh:K�ubjn  )��}�(h�+Pointer acceleration curves for trackpoints�h]�h�+Pointer acceleration curves for trackpoints�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jm  h8hkh:K�h jy  ubeh!}�(h#]��id5�ah%]�h']�h)]�h+]�j�  �center�uh0jW  h:K�h j�  hhh8hkubh�)��}�(h�TThe image above shows the trackpoint acceleration profile for the speed in
units/ms.�h]�h�TThe image above shows the trackpoint acceleration profile for the speed in
units/ms.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh^)��}�(h�.. _ptraccel-profile-flat:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�ptraccel-profile-flat�uh0h]h:K�h j�  hhh8hkubeh!}�(h#]�(�#pointer-acceleration-on-trackpoints�j�  eh%]�h']�(�#pointer acceleration on trackpoints��ptraccel-trackpoint�eh)]�h+]�uh0hlh hnhhh8hkh:K�jR  }�j�  j�  sjT  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�%The flat pointer acceleration profile�h]�h�%The flat pointer acceleration profile�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K�ubh�)��}�(h��In a flat profile, the acceleration factor is constant regardless of the
velocity of the pointer and each delta (dx, dy) results in an accelerated delta
(dx * factor, dy * factor). This provides 1:1 movement between the device
and the pointer on-screen.�h]�h��In a flat profile, the acceleration factor is constant regardless of the
velocity of the pointer and each delta (dx, dy) results in an accelerated delta
(dx * factor, dy * factor). This provides 1:1 movement between the device
and the pointer on-screen.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh^)��}�(h�.. _ptraccel-tablet:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�ptraccel-tablet�uh0h]h:K�h j�  hhh8hkubeh!}�(h#]�(�%the-flat-pointer-acceleration-profile�j�  eh%]�h']�(�%the flat pointer acceleration profile��ptraccel-profile-flat�eh)]�h+]�uh0hlh hnhhh8hkh:K�jR  }�j�  j�  sjT  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Pointer acceleration on tablets�h]�h�Pointer acceleration on tablets�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K�ubh�)��}�(h��Pointer acceleration for relative motion on tablet devices is a flat
acceleration, with the speed setting slowing down or speeding up the pointer
motion by a constant factor. Tablets do not allow for switchable profiles.�h]�h��Pointer acceleration for relative motion on tablet devices is a flat
acceleration, with the speed setting slowing down or speeding up the pointer
motion by a constant factor. Tablets do not allow for switchable profiles.�����}�(hj  h j	  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubeh!}�(h#]�(�pointer-acceleration-on-tablets�j�  eh%]�h']�(�pointer acceleration on tablets��ptraccel-tablet�eh)]�h+]�uh0hlh hnhhh8hkh:K�jR  }�j  j�  sjT  }�j�  j�  subeh!}�(h#]�(hj�id1�eh%]�h']�(�pointer acceleration��pointer-acceleration�eh)]�h+]�uh0hlh hhhh8hkh:KjR  }�j(  h_sjT  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jP  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_ah�]�h�ajH  ]�j>  aj�  ]�j�  aj�  ]�j�  aj�  ]�j�  aj9  ]�j/  aj�  ]�j�  aj�  ]�j�  aj�  ]�j�  au�nameids�}�(j(  hjj'  j$  jO  h�jN  jK  j�  jH  j�  j�  j  j�  j   j�  j�  j�  j�  j�  j@  j�  j?  j<  j�  j9  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j  j�  j  j  u�	nametypes�}�(j(  �j'  NjO  �jN  Nj�  �j�  Nj  �j   Nj�  �j�  Nj@  �j?  Nj�  �j�  Nj�  �j�  Nj�  �j�  Nj  �j  Nuh#}�(hjhnj$  hnh�h�jK  h�jH  jV  j�  jV  j�  j�  j�  j�  j�  j  j�  j  j�  j�  j<  j�  j9  jE  j�  jE  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j  j�  j  jY  j  j�  j�  j�  j�  jy  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�:Hyperlink target "pointer-acceleration" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�7Hyperlink target "ptraccel-profiles" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�7Hyperlink target "ptraccel-velocity" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K"uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�5Hyperlink target "ptraccel-factor" is not referenced.�����}�(hhh j
  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K=uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�5Hyperlink target "ptraccel-linear" is not referenced.�����}�(hhh j$  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j!  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�KRuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�6Hyperlink target "ptraccel-low-dpi" is not referenced.�����}�(hhh j>  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j;  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Knuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�7Hyperlink target "ptraccel-touchpad" is not referenced.�����}�(hhh jX  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jU  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K�uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�9Hyperlink target "ptraccel-trackpoint" is not referenced.�����}�(hhh jr  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jo  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K�uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�;Hyperlink target "ptraccel-profile-flat" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K�uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�5Hyperlink target "ptraccel-tablet" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K�uh0j�  ube�transformer�N�
decoration�Nhhub.