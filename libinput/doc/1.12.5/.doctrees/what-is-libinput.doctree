���n      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`7d25d9e`�h]�h �	reference���)��}�(h�git commit 7d25d9e�h]�h �Text����git commit 7d25d9e�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/7d25d9e�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�^.. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4b9dfe32f0>`


�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�h]�h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4b9dfe32f0>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _what_is_libinput:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��what-is-libinput�uh0h]h:Kh hhhh8�</home/whot/code/libinput/build/doc/user/what-is-libinput.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�What is libinput?�h]�h�What is libinput?�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h�fThis page describes what libinput is, but more importantly it also describes
what libinput is **not**.�h]�(h�^This page describes what libinput is, but more importantly it also describes
what libinput is �����}�(h�^This page describes what libinput is, but more importantly it also describes
what libinput is �h h�hhh8Nh:Nubh �strong���)��}�(h�**not**�h]�h�not�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�.�����}�(h�.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�.. _what_libinput_is:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�what-libinput-is�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�What libinput is�h]�h�What libinput is�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh h�hhh8hkh:Kubh�)��}�(hX  libinput is an input stack for processes that need to provide events from
commonly used input devices. That includes mice, keyboards, touchpads,
touchscreens and graphics tablets. libinput handles device-specific quirks
and provides an easy-to-use API to receive events from devices.�h]�hX  libinput is an input stack for processes that need to provide events from
commonly used input devices. That includes mice, keyboards, touchpads,
touchscreens and graphics tablets. libinput handles device-specific quirks
and provides an easy-to-use API to receive events from devices.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh�)��}�(hXn  libinput is designed to handle all input devices available on a system but
it is possible to limit which devices libinput has access to.
For example, the use of xf86-input-libinput depends on xorg.conf snippets
for specific devices. But libinput works best if it handles all input
devices as this allows for smarter handling of features that affect multiple
devices.�h]�hXn  libinput is designed to handle all input devices available on a system but
it is possible to limit which devices libinput has access to.
For example, the use of xf86-input-libinput depends on xorg.conf snippets
for specific devices. But libinput works best if it handles all input
devices as this allows for smarter handling of features that affect multiple
devices.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh�)��}�(hX  libinput restricts device-specific features to those devices that require
those features. One example for this are the top software buttons on the
touchpad in the Lenovo T440. While there may be use-cases for providing top
software buttons on other devices, libinput does not do so.�h]�hX  libinput restricts device-specific features to those devices that require
those features. One example for this are the top software buttons on the
touchpad in the Lenovo T440. While there may be use-cases for providing top
software buttons on other devices, libinput does not do so.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh�)��}�(h��`This introductory blog post from 2015
<https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>`_
describes some of the motivations.�h]�(h)��}�(h�t`This introductory blog post from 2015
<https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>`_�h]�h�%This introductory blog post from 2015�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]��name��%This introductory blog post from 2015��refuri��Ihttps://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html�uh0hh h�ubh^)��}�(h�L
<https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>�h]�h!}�(h#]��%this-introductory-blog-post-from-2015�ah%]�h']��%this introductory blog post from 2015�ah)]�h+]��refuri�j  uh0h]�
referenced�Kh h�ubh�#
describes some of the motivations.�����}�(h�#
describes some of the motivations.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K"h h�hhubh^)��}�(h�.. _what_libinput_is_not:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�what-libinput-is-not�uh0h]h:K+h h�hhh8hkubeh!}�(h#]�(h��id2�eh%]�h']�(�what libinput is��what_libinput_is�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j-  h�s�expect_referenced_by_id�}�h�h�subhm)��}�(hhh]�(hr)��}�(h�What libinput is not�h]�h�What libinput is not�����}�(hj9  h j7  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j4  hhh8hkh:K*ubh�)��}�(hXr  libinput is **not** a project to support experimental devices. Unless a
device is commonly available off-the-shelf, libinput will not support this
device. libinput can serve as a useful base for getting experimental devices
enabled and reduce the amount of boilerplate required. But such support will
not land in libinput master until the devices are commonly available.�h]�(h�libinput is �����}�(h�libinput is �h jE  hhh8Nh:Nubh�)��}�(h�**not**�h]�h�not�����}�(hhh jN  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jE  ubhX_   a project to support experimental devices. Unless a
device is commonly available off-the-shelf, libinput will not support this
device. libinput can serve as a useful base for getting experimental devices
enabled and reduce the amount of boilerplate required. But such support will
not land in libinput master until the devices are commonly available.�����}�(hX_   a project to support experimental devices. Unless a
device is commonly available off-the-shelf, libinput will not support this
device. libinput can serve as a useful base for getting experimental devices
enabled and reduce the amount of boilerplate required. But such support will
not land in libinput master until the devices are commonly available.�h jE  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K,h j4  hhubh�)��}�(hXT  libinput is **not** a box of legos. It does not provide the pieces to
assemble a selection of features. Many features can be disabled through
configuration options, but some features are hardcoded and/or only available
on some devices. There are plenty of use-cases to provide niche features,
but libinput is not the place to support these.�h]�(h�libinput is �����}�(h�libinput is �h jg  hhh8Nh:Nubh�)��}�(h�**not**�h]�h�not�����}�(hhh jp  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jg  ubhXA   a box of legos. It does not provide the pieces to
assemble a selection of features. Many features can be disabled through
configuration options, but some features are hardcoded and/or only available
on some devices. There are plenty of use-cases to provide niche features,
but libinput is not the place to support these.�����}�(hXA   a box of legos. It does not provide the pieces to
assemble a selection of features. Many features can be disabled through
configuration options, but some features are hardcoded and/or only available
on some devices. There are plenty of use-cases to provide niche features,
but libinput is not the place to support these.�h jg  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K2h j4  hhubh�)��}�(hXu  libinput is **not** a showcase for features. There are a lot of potential
features that could be provided on input devices. But unless they have
common usage, libinput is not the place to implement them. Every feature
multiplies the maintenance effort, any feature that is provided but unused
is a net drain on the already sparse developer resources libinput has
available.�h]�(h�libinput is �����}�(h�libinput is �h j�  hhh8Nh:Nubh�)��}�(h�**not**�h]�h�not�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubhXb   a showcase for features. There are a lot of potential
features that could be provided on input devices. But unless they have
common usage, libinput is not the place to implement them. Every feature
multiplies the maintenance effort, any feature that is provided but unused
is a net drain on the already sparse developer resources libinput has
available.�����}�(hXb   a showcase for features. There are a lot of potential
features that could be provided on input devices. But unless they have
common usage, libinput is not the place to implement them. Every feature
multiplies the maintenance effort, any feature that is provided but unused
is a net drain on the already sparse developer resources libinput has
available.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K8h j4  hhubh�)��}�(hX  libinput is boring. It does not intend to break new grounds on how devices
are handled. Instead, it takes best practice and the common use-cases and
provides it in an easy-to-consume package for compositors or other processes
that need those interactions typically expected by users.�h]�hX  libinput is boring. It does not intend to break new grounds on how devices
are handled. Instead, it takes best practice and the common use-cases and
provides it in an easy-to-consume package for compositors or other processes
that need those interactions typically expected by users.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K?h j4  hhubh^)��}�(h�.. _libinput-wayland:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�libinput-wayland�uh0h]h:KIh j4  hhh8hkubeh!}�(h#]�(j&  �id3�eh%]�h']�(�what libinput is not��what_libinput_is_not�eh)]�h+]�uh0hlh hnhhh8hkh:K*j0  }�j�  j  sj2  }�j&  j  subhm)��}�(hhh]�(hr)��}�(h�libinput and Wayland�h]�h�libinput and Wayland�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:KHubh�)��}�(h��libinput is not used directly by Wayland applications, it is an input stack
used by the compositor. The typical software stack for a system running
Wayland is:�h]�h��libinput is not used directly by Wayland applications, it is an input stack
used by the compositor. The typical software stack for a system running
Wayland is:�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KJh j�  hhub�sphinx.ext.graphviz��graphviz���)��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]��code�X�  digraph stack
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  subgraph cluster_2 {
	  label="Kernel";
	  event0 [label="/dev/input/event0"]
	  event1 [label="/dev/input/event1"]
  }

  subgraph cluster_0 {
	  label="Compositor process";
	  libinput;
  }

  client [label="Wayland client"];

  event0 -> libinput;
  event1 -> libinput;
  libinput -> client [ltail=cluster_0 label="Wayland protocol"];
}
��options�}�uh0j�  h j�  hhh8hkh:KOubh�)��}�(hX3  The Wayland compositor may be Weston, mutter, KWin, etc. Note that
Wayland encourages the use of toolkits, so the Wayland client (your
application) does not usually talk directly to the compositor but rather
employs a toolkit (e.g. GTK) to do so. The Wayland client does not know
whether libinput is in use.�h]�hX3  The Wayland compositor may be Weston, mutter, KWin, etc. Note that
Wayland encourages the use of toolkits, so the Wayland client (your
application) does not usually talk directly to the compositor but rather
employs a toolkit (e.g. GTK) to do so. The Wayland client does not know
whether libinput is in use.�����}�(hj   h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KPh j�  hhubh�)��}�(h��libinput is not a requirement for Wayland or even a Wayland compositor.
There are some specialized compositors that do not need or want libinput.�h]�h��libinput is not a requirement for Wayland or even a Wayland compositor.
There are some specialized compositors that do not need or want libinput.�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KVh j�  hhubh^)��}�(h�.. _libinput-xorg:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�libinput-xorg�uh0h]h:K^h j�  hhh8hkubeh!}�(h#]�(�libinput-and-wayland�j�  eh%]�h']�(�libinput and wayland��libinput-wayland�eh)]�h+]�uh0hlh hnhhh8hkh:KHj0  }�j+  j�  sj2  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�libinput and X.Org�h]�h�libinput and X.Org�����}�(hj5  h j3  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j0  hhh8hkh:K]ubh�)��}�(h��libinput is not used directly by X applications but rather through the
custom xf86-input-libinput driver. The simplified software stack for a
system running X.Org is:�h]�h��libinput is not used directly by X applications but rather through the
custom xf86-input-libinput driver. The simplified software stack for a
system running X.Org is:�����}�(hjC  h jA  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K_h j0  hhubj�  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�j�  X�  digraph stack
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  subgraph cluster_2 {
	  label="Kernel";
	  event0 [label="/dev/input/event0"]
	  event1 [label="/dev/input/event1"]
  }

  subgraph cluster_0 {
	  label="X server process";
	  subgraph cluster_1 {
		  label="xf86-input-libinput"
		  libinput;
	  }
  }

  libinput;
  client [label="X11 client"];

  event0 -> libinput;
  event1 -> libinput;
  libinput -> client [ltail=cluster_0 label="X protocol"];
}
�j�  }�uh0j�  h j0  hhh8hkh:Kdubh�)��}�(h��libinput is not employed directly by the X server but by the
xf86-input-libinput driver instead. That driver is loaded by the server
on demand, depending on the xorg.conf.d configuration snippets. The X client
does not know whether libinput is in use.�h]�h��libinput is not employed directly by the X server but by the
xf86-input-libinput driver instead. That driver is loaded by the server
on demand, depending on the xorg.conf.d configuration snippets. The X client
does not know whether libinput is in use.�����}�(hj\  h jZ  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Keh j0  hhubh�)��}�(h��libinput and xf86-input-libinput are not a requirement, the driver will only
handle those devices explicitly assigned through an xorg.conf.d snippets. It
is possible to mix xf86-input-libinput with other X.Org drivers.�h]�h��libinput and xf86-input-libinput are not a requirement, the driver will only
handle those devices explicitly assigned through an xorg.conf.d snippets. It
is possible to mix xf86-input-libinput with other X.Org drivers.�����}�(hjj  h jh  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kjh j0  hhubeh!}�(h#]�(�libinput-and-x-org�j$  eh%]�h']�(�libinput and x.org��libinput-xorg�eh)]�h+]�uh0hlh hnhhh8hkh:K]j0  }�j|  j  sj2  }�j$  j  subhm)��}�(hhh]�(hr)��}�(h�Device types�h]�h�Device types�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:Kpubh�)��}�(hXo  libinput handles all common devices used to interact with a desktop system.
This includes mice, keyboards, touchscreens, touchpads and graphics tablets.
libinput does not expose the device type to the caller, it solely provides
capabilities and the attached features (see
`this blog post <https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>`_).�h]�(hX  libinput handles all common devices used to interact with a desktop system.
This includes mice, keyboards, touchscreens, touchpads and graphics tablets.
libinput does not expose the device type to the caller, it solely provides
capabilities and the attached features (see
�����}�(hX  libinput handles all common devices used to interact with a desktop system.
This includes mice, keyboards, touchscreens, touchpads and graphics tablets.
libinput does not expose the device type to the caller, it solely provides
capabilities and the attached features (see
�h j�  hhh8Nh:Nubh)��}�(h�]`this blog post <https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>`_�h]�h�this blog post�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��name��this blog post�j  �Ihttps://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html�uh0hh j�  ubh^)��}�(h�L <https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>�h]�h!}�(h#]��this-blog-post�ah%]�h']��this blog post�ah)]�h+]��refuri�j�  uh0h]j  Kh j�  ubh�).�����}�(h�).�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Krh j�  hhubh�)��}�(hX  For example, a touchpad in libinput is a device that provides pointer
events, gestures and has a number of :ref:`config_options` such as
:ref:`tapping`. A caller may present the device as touchpad to the user, or
simply as device with a config knob to enable or disable tapping.�h]�(h�kFor example, a touchpad in libinput is a device that provides pointer
events, gestures and has a number of �����}�(h�kFor example, a touchpad in libinput is a device that provides pointer
events, gestures and has a number of �h j�  hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`config_options`�h]�h �inline���)��}�(hj�  h]�h�config_options�����}�(hhh j�  ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0j�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit���	reftarget��config_options��refdoc��what-is-libinput��refwarn��uh0j�  h8hkh:Kxh j�  ubh�	 such as
�����}�(h�	 such as
�h j�  hhh8Nh:Nubj�  )��}�(h�:ref:`tapping`�h]�j�  )��}�(hj�  h]�h�tapping�����}�(hhh j�  ubah!}�(h#]�h%]�(j�  �std��std-ref�eh']�h)]�h+]�uh0j�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j  �refexplicit��j�  �tapping�j�  j�  j�  �uh0j�  h8hkh:Kxh j�  ubh�. A caller may present the device as touchpad to the user, or
simply as device with a config knob to enable or disable tapping.�����}�(h�. A caller may present the device as touchpad to the user, or
simply as device with a config knob to enable or disable tapping.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kxh j�  hhubhm)��}�(hhh]�(hr)��}�(h�Handled device types�h]�h�Handled device types�����}�(hj(  h j&  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j#  hhh8hkh:Kubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�:ref:`Touchpads`�h]�h�)��}�(hj=  h]�j�  )��}�(hj=  h]�j�  )��}�(hj=  h]�h�	Touchpads�����}�(hhh jE  ubah!}�(h#]�h%]�(j�  �std��std-ref�eh']�h)]�h+]�uh0j�  h jB  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jO  �refexplicit��j�  �	touchpads�j�  j�  j�  �uh0j�  h8hkh:K�h j?  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j;  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j9  h j6  hhh8hkh:Nubj:  )��}�(h�Touchscreens�h]�h�)��}�(hjm  h]�h�Touchscreens�����}�(hjm  h jo  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h jk  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j9  h j6  hhh8hkh:Nubj:  )��}�(h�Mice�h]�h�)��}�(hj�  h]�h�Mice�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j9  h j6  hhh8hkh:Nubj:  )��}�(h�	Keyboards�h]�h�)��}�(hj�  h]�h�	Keyboards�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j9  h j6  hhh8hkh:Nubj:  )��}�(h�JVirtual absolute pointing devices such as those used by QEMU or VirtualBox�h]�h�)��}�(hj�  h]�h�JVirtual absolute pointing devices such as those used by QEMU or VirtualBox�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j9  h j6  hhh8hkh:Nubj:  )��}�(h�,Switches (Lid Switch and Tablet Mode switch)�h]�h�)��}�(hj�  h]�h�,Switches (Lid Switch and Tablet Mode switch)�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j9  h j6  hhh8hkh:Nubj:  )��}�(h�Graphics tablets�h]�h�)��}�(hj�  h]�h�Graphics tablets�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j9  h j6  hhh8hkh:Nubj:  )��}�(h�:ref:`Trackpoints`
�h]�h�)��}�(h�:ref:`Trackpoints`�h]�j�  )��}�(hj�  h]�j�  )��}�(hj�  h]�h�Trackpoints�����}�(hhh j   ubah!}�(h#]�h%]�(j�  �std��std-ref�eh']�h)]�h+]�uh0j�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j
  �refexplicit��j�  �trackpoints�j�  j�  j�  �uh0j�  h8hkh:K�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j9  h j6  hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]��bullet��-�uh0j4  h8hkh:K�h j#  hhubh�)��}�(h�|If a device falls into one of the above categories but does not work as
expected, please :ref:`file a bug <reporting_bugs>`.�h]�(h�YIf a device falls into one of the above categories but does not work as
expected, please �����}�(h�YIf a device falls into one of the above categories but does not work as
expected, please �h j.  hhh8Nh:Nubj�  )��}�(h�":ref:`file a bug <reporting_bugs>`�h]�j�  )��}�(hj9  h]�h�
file a bug�����}�(hhh j;  ubah!}�(h#]�h%]�(j�  �std��std-ref�eh']�h)]�h+]�uh0j�  h j7  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jE  �refexplicit��j�  �reporting_bugs�j�  j�  j�  �uh0j�  h8hkh:K�h j.  ubh�.�����}�(hh�h j.  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j#  hhubeh!}�(h#]��handled-device-types�ah%]�h']��handled device types�ah)]�h+]�uh0hlh j�  hhh8hkh:Kubhm)��}�(hhh]�(hr)��}�(h�Unhandled device types�h]�h�Unhandled device types�����}�(hjl  h jj  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jg  hhh8hkh:K�ubh�)��}�(h��libinput does not handle some devices. The primary reason is that these
device have no clear interaction with a desktop environment.�h]�h��libinput does not handle some devices. The primary reason is that these
device have no clear interaction with a desktop environment.�����}�(hjz  h jx  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h jg  hhubh �definition_list���)��}�(hhh]�h �definition_list_item���)��}�(hX�  Joysticks:
Joysticks have one or more axes and one or more buttons. Beyond that it is
difficult to find common ground between joysticks and much of the
interaction is application-specific, not system-specific. libinput does not
provide support for joysticks for that reason, any abstraction libinput
would provide for joysticks would be so generic that libinput would
merely introduce complexity and processing delays for no real benefit.�h]�(h �term���)��}�(h�
Joysticks:�h]�h�
Joysticks:�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:K�h j�  ubh �
definition���)��}�(hhh]�h�)��}�(hX�  Joysticks have one or more axes and one or more buttons. Beyond that it is
difficult to find common ground between joysticks and much of the
interaction is application-specific, not system-specific. libinput does not
provide support for joysticks for that reason, any abstraction libinput
would provide for joysticks would be so generic that libinput would
merely introduce complexity and processing delays for no real benefit.�h]�hX�  Joysticks have one or more axes and one or more buttons. Beyond that it is
difficult to find common ground between joysticks and much of the
interaction is application-specific, not system-specific. libinput does not
provide support for joysticks for that reason, any abstraction libinput
would provide for joysticks would be so generic that libinput would
merely introduce complexity and processing delays for no real benefit.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:K�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h jg  hhh8hkh:Nubeh!}�(h#]��unhandled-device-types�ah%]�h']��unhandled device types�ah)]�h+]�uh0hlh j�  hhh8hkh:K�ubeh!}�(h#]��device-types�ah%]�h']��device types�ah)]�h+]�uh0hlh hnhhh8hkh:Kpubeh!}�(h#]�(hj�id1�eh%]�h']�(�what is libinput?��what_is_libinput�eh)]�h+]�uh0hlh hhhh8hkh:Kj0  }�j�  h_sj2  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_ah�]�h�aj&  ]�j  aj�  ]�j�  aj$  ]�j  au�nameids�}�(j�  hjj�  j�  j-  h�j,  j)  j  j	  j�  j&  j�  j�  j+  j�  j*  j'  j|  j$  j{  jx  j�  j�  j�  j�  jd  ja  j�  j�  u�	nametypes�}�(j�  �j�  Nj-  �j,  Nj  �j�  �j�  Nj+  �j*  Nj|  �j{  Nj�  Nj�  �jd  Nj�  Nuh#}�(hjhnj�  hnh�h�j)  h�j	  j  j&  j4  j�  j4  j�  j�  j'  j�  j$  j0  jx  j0  j�  j�  j�  j�  ja  j#  j�  jg  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�6Hyperlink target "what-is-libinput" is not referenced.�����}�(hhh jj  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jg  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0je  ubjf  )��}�(hhh]�h�)��}�(hhh]�h�6Hyperlink target "what-libinput-is" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j  �source�hk�line�Kuh0je  ubjf  )��}�(hhh]�h�)��}�(hhh]�h�:Hyperlink target "what-libinput-is-not" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j  �source�hk�line�K+uh0je  ubjf  )��}�(hhh]�h�)��}�(hhh]�h�6Hyperlink target "libinput-wayland" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j  �source�hk�line�KIuh0je  ubjf  )��}�(hhh]�h�)��}�(hhh]�h�3Hyperlink target "libinput-xorg" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j  �source�hk�line�K^uh0je  ube�transformer�N�
decoration�Nhhub.