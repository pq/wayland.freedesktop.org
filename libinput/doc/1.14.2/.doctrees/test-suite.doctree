����      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _test-suite:�h]�h}�(h]�h]�h]�h]�h]��refid��
test-suite�uhhhKhhhhh�6/home/whot/code/libinput/build/doc/user/test-suite.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�libinput test suite�h]�h �Text����libinput test suite�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h�1libinput's primary test suite can be invoked with�h]�h9�3libinput’s primary test suite can be invoked with�����}�(hhHhhFhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh �literal_block���)��}�(h�%$ sudo ./builddir/libinput-test-suite�h]�h9�%$ sudo ./builddir/libinput-test-suite�����}�(hhhhVubah}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhhThh,hKhh/hhubhE)��}�(hX�  When developing libinput, the ``libinput-test-suite`` should always be
run to check for behavior changes and/or regressions. For quick iteration,
the number of tests to run can be filtered, see :ref:`test-filtering`.
This allows for developers to verify a subset of tests (e.g.
touchpad tap-to-click) while hacking on that specific feature and only run
the full suite when development is done finished.�h]�(h9�When developing libinput, the �����}�(h�When developing libinput, the �hhfhhhNhNubh �literal���)��}�(h�``libinput-test-suite``�h]�h9�libinput-test-suite�����}�(h�libinput-test-suite�hhqubah}�(h]�h]�h]�h]�h]�uhhohhfubh9�� should always be
run to check for behavior changes and/or regressions. For quick iteration,
the number of tests to run can be filtered, see �����}�(h�� should always be
run to check for behavior changes and/or regressions. For quick iteration,
the number of tests to run can be filtered, see �hhfhhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`test-filtering`�h]�h �inline���)��}�(h�test-filtering�h]�h9�test-filtering�����}�(hhhh�ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��refdoc��
test-suite��	refdomain�h��reftype��ref��refexplicit���refwarn���	reftarget��test-filtering�uhh�hh,hKhhfubh9��.
This allows for developers to verify a subset of tests (e.g.
touchpad tap-to-click) while hacking on that specific feature and only run
the full suite when development is done finished.�����}�(h��.
This allows for developers to verify a subset of tests (e.g.
touchpad tap-to-click) while hacking on that specific feature and only run
the full suite when development is done finished.�hhfhhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh �note���)��}�(h��The test suite relies on udev and the kernel, specifically uinput.
It creates virtual input devices and replays the events. This may
interfere with your running session. The test suite is not suitable
for running inside containers.�h]�hE)��}�(h��The test suite relies on udev and the kernel, specifically uinput.
It creates virtual input devices and replays the events. This may
interfere with your running session. The test suite is not suitable
for running inside containers.�h]�h9��The test suite relies on udev and the kernel, specifically uinput.
It creates virtual input devices and replays the events. This may
interfere with your running session. The test suite is not suitable
for running inside containers.�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh/hhhh,hNubhE)��}�(hX   In addition, libinput ships with a set of (primarily janitorial) tests that
must pass for any merge request. These tests are invoked by calling
``meson test -C builddir`` (or ``ninja test``). The ``libinput-test-suite`` is
part of that test set by default.�h]�(h9��In addition, libinput ships with a set of (primarily janitorial) tests that
must pass for any merge request. These tests are invoked by calling
�����}�(h��In addition, libinput ships with a set of (primarily janitorial) tests that
must pass for any merge request. These tests are invoked by calling
�hh�hhhNhNubhp)��}�(h�``meson test -C builddir``�h]�h9�meson test -C builddir�����}�(h�meson test -C builddir�hh�ubah}�(h]�h]�h]�h]�h]�uhhohh�ubh9� (or �����}�(h� (or �hh�hhhNhNubhp)��}�(h�``ninja test``�h]�h9�
ninja test�����}�(h�
ninja test�hh�ubah}�(h]�h]�h]�h]�h]�uhhohh�ubh9�). The �����}�(h�). The �hh�hhhNhNubhp)��}�(h�``libinput-test-suite``�h]�h9�libinput-test-suite�����}�(h�libinput-test-suite�hj  ubah}�(h]�h]�h]�h]�h]�uhhohh�ubh9�% is
part of that test set by default.�����}�(h�% is
part of that test set by default.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(h�qThe upstream CI runs all these tests but not the ``libinput-test-suite``.
This CI is run for every merge request.�h]�(h9�1The upstream CI runs all these tests but not the �����}�(h�1The upstream CI runs all these tests but not the �hj  hhhNhNubhp)��}�(h�``libinput-test-suite``�h]�h9�libinput-test-suite�����}�(h�libinput-test-suite�hj'  ubah}�(h]�h]�h]�h]�h]�uhhohj  ubh9�).
This CI is run for every merge request.�����}�(h�).
This CI is run for every merge request.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _test-job-control:�h]�h}�(h]�h]�h]�h]�h]�h*�test-job-control�uhhhK&hh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�Job control in the test suite�h]�h9�Job control in the test suite�����}�(hjQ  hjO  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hjL  hhhh,hK%ubhE)��}�(hX�  The test suite runner has a make-like job control enabled by the ``-j`` or
``--jobs`` flag and will fork off as many parallel processes as given by this
flag. The default if unspecified is 8. When debugging a specific test case
failure it is recommended to employ test filtures (see :ref:`test-filtering`)
and disable parallel tests. The test suite automatically disables parallel
make when run in gdb.�h]�(h9�AThe test suite runner has a make-like job control enabled by the �����}�(h�AThe test suite runner has a make-like job control enabled by the �hj]  hhhNhNubhp)��}�(h�``-j``�h]�h9�-j�����}�(h�-j�hjf  ubah}�(h]�h]�h]�h]�h]�uhhohj]  ubh9� or
�����}�(h� or
�hj]  hhhNhNubhp)��}�(h�
``--jobs``�h]�h9�--jobs�����}�(h�--jobs�hjz  ubah}�(h]�h]�h]�h]�h]�uhhohj]  ubh9�� flag and will fork off as many parallel processes as given by this
flag. The default if unspecified is 8. When debugging a specific test case
failure it is recommended to employ test filtures (see �����}�(h�� flag and will fork off as many parallel processes as given by this
flag. The default if unspecified is 8. When debugging a specific test case
failure it is recommended to employ test filtures (see �hj]  hhhNhNubh�)��}�(h�:ref:`test-filtering`�h]�h�)��}�(h�test-filtering�h]�h9�test-filtering�����}�(hhhj�  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]��refdoc�h��	refdomain�j�  �reftype��ref��refexplicit���refwarn��h��test-filtering�uhh�hh,hK'hj]  ubh9�b)
and disable parallel tests. The test suite automatically disables parallel
make when run in gdb.�����}�(h�b)
and disable parallel tests. The test suite automatically disables parallel
make when run in gdb.�hj]  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK'hjL  hhubh)��}�(h�.. _test-config:�h]�h}�(h]�h]�h]�h]�h]�h*�test-config�uhhhK3hjL  hhhh,ubeh}�(h]�(�job-control-in-the-test-suite�jK  eh]�h]�(�job control in the test suite��test-job-control�eh]�h]�uhh-hh/hhhh,hK%�expect_referenced_by_name�}�j�  jA  s�expect_referenced_by_id�}�jK  jA  subh.)��}�(hhh]�(h3)��}�(h�"X.Org config to avoid interference�h]�h9�"X.Org config to avoid interference�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK2ubhE)��}�(h��uinput devices created by the test suite are usually recognised by X as
input devices. All events sent through these devices will generate X events
and interfere with your desktop.�h]�h9��uinput devices created by the test suite are usually recognised by X as
input devices. All events sent through these devices will generate X events
and interfere with your desktop.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK4hj�  hhubhE)��}�(h��Copy the file ``$srcdir/test/50-litest.conf`` into your ``/etc/X11/xorg.conf.d``
and restart X. This will ignore any litest devices and thus not interfere
with your desktop.�h]�(h9�Copy the file �����}�(h�Copy the file �hj�  hhhNhNubhp)��}�(h�``$srcdir/test/50-litest.conf``�h]�h9�$srcdir/test/50-litest.conf�����}�(h�$srcdir/test/50-litest.conf�hj�  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9� into your �����}�(h� into your �hj�  hhhNhNubhp)��}�(h�``/etc/X11/xorg.conf.d``�h]�h9�/etc/X11/xorg.conf.d�����}�(h�/etc/X11/xorg.conf.d�hj  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9�]
and restart X. This will ignore any litest devices and thus not interfere
with your desktop.�����}�(h�]
and restart X. This will ignore any litest devices and thus not interfere
with your desktop.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK8hj�  hhubh)��}�(h�.. _test-root:�h]�h}�(h]�h]�h]�h]�h]�h*�	test-root�uhhhKAhj�  hhhh,ubeh}�(h]�(�"x-org-config-to-avoid-interference�j�  eh]�h]�(�"x.org config to avoid interference��test-config�eh]�h]�uhh-hh/hhhh,hK2j�  }�j9  j�  sj�  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�!Permissions required to run tests�h]�h9�!Permissions required to run tests�����}�(hjC  hjA  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj>  hhhh,hK@ubhE)��}�(hX7  Most tests require the creation of uinput devices and access to the
resulting ``/dev/input/eventX`` nodes. Some tests require temporary udev rules.
**This usually requires the tests to be run as root**. If not run as
root, the test suite runner will exit with status 77, an exit status
interpreted as "skipped".�h]�(h9�NMost tests require the creation of uinput devices and access to the
resulting �����}�(h�NMost tests require the creation of uinput devices and access to the
resulting �hjO  hhhNhNubhp)��}�(h�``/dev/input/eventX``�h]�h9�/dev/input/eventX�����}�(h�/dev/input/eventX�hjX  ubah}�(h]�h]�h]�h]�h]�uhhohjO  ubh9�1 nodes. Some tests require temporary udev rules.
�����}�(h�1 nodes. Some tests require temporary udev rules.
�hjO  hhhNhNubh �strong���)��}�(h�5**This usually requires the tests to be run as root**�h]�h9�1This usually requires the tests to be run as root�����}�(h�1This usually requires the tests to be run as root�hjn  ubah}�(h]�h]�h]�h]�h]�uhjl  hjO  ubh9�r. If not run as
root, the test suite runner will exit with status 77, an exit status
interpreted as “skipped”.�����}�(h�n. If not run as
root, the test suite runner will exit with status 77, an exit status
interpreted as "skipped".�hjO  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKBhj>  hhubh)��}�(h�.. _test-filtering:�h]�h}�(h]�h]�h]�h]�h]�h*�test-filtering�uhhhKMhj>  hhhh,ubeh}�(h]�(�!permissions-required-to-run-tests�j2  eh]�h]�(�!permissions required to run tests��	test-root�eh]�h]�uhh-hh/hhhh,hK@j�  }�j�  j(  sj�  }�j2  j(  subh.)��}�(hhh]�(h3)��}�(h�Selective running of tests�h]�h9�Selective running of tests�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKLubhE)��}�(hX�  litest's tests are grouped into test groups, test names and devices. A test
group is e.g.  "touchpad:tap" and incorporates all tapping-related tests for
touchpads. Each test function is (usually) run with one or more specific
devices. The ``--list`` commandline argument shows the list of suites and
tests. This is useful when trying to figure out if a specific test is
run for a device.�h]�(h9��litest’s tests are grouped into test groups, test names and devices. A test
group is e.g.  “touchpad:tap” and incorporates all tapping-related tests for
touchpads. Each test function is (usually) run with one or more specific
devices. The �����}�(h��litest's tests are grouped into test groups, test names and devices. A test
group is e.g.  "touchpad:tap" and incorporates all tapping-related tests for
touchpads. Each test function is (usually) run with one or more specific
devices. The �hj�  hhhNhNubhp)��}�(h�
``--list``�h]�h9�--list�����}�(h�--list�hj�  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9�� commandline argument shows the list of suites and
tests. This is useful when trying to figure out if a specific test is
run for a device.�����}�(h�� commandline argument shows the list of suites and
tests. This is useful when trying to figure out if a specific test is
run for a device.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKNhj�  hhubhU)��}�(hX�  $ ./builddir/libinput-test-suite --list
...
pointer:left-handed:
   pointer_left_handed_during_click_multiple_buttons:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           magicmouse
   pointer_left_handed_during_click:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           litest-magicmouse-device
   pointer_left_handed:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
...�h]�h9X�  $ ./builddir/libinput-test-suite --list
...
pointer:left-handed:
   pointer_left_handed_during_click_multiple_buttons:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           magicmouse
   pointer_left_handed_during_click:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           litest-magicmouse-device
   pointer_left_handed:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
...�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�hdheuhhThh,hKXhj�  hhubhE)��}�(hX,  In the above example, the "pointer:left-handed" suite contains multiple
tests, e.g. "pointer_left_handed_during_click" (this is also the function
name of the test, making it easy to grep for). This particular test is run
for various devices including the trackpoint device and the magic mouse
device.�h]�h9X4  In the above example, the “pointer:left-handed” suite contains multiple
tests, e.g. “pointer_left_handed_during_click” (this is also the function
name of the test, making it easy to grep for). This particular test is run
for various devices including the trackpoint device and the magic mouse
device.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhj�  hhubhE)��}�(h��The "no device" entry signals that litest does not instantiate a uinput
device for a specific test (though the test itself may
instantiate one).�h]�h9��The “no device” entry signals that litest does not instantiate a uinput
device for a specific test (though the test itself may
instantiate one).�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhE)��}�(h��The ``--filter-test`` argument enables selective running of tests through
basic shell-style function name matching. For example:�h]�(h9�The �����}�(h�The �hj�  hhhNhNubhp)��}�(h�``--filter-test``�h]�h9�--filter-test�����}�(h�--filter-test�hj  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9�k argument enables selective running of tests through
basic shell-style function name matching. For example:�����}�(h�k argument enables selective running of tests through
basic shell-style function name matching. For example:�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhU)��}�(h�:$ ./builddir/libinput-test-suite --filter-test="*1fg_tap*"�h]�h9�:$ ./builddir/libinput-test-suite --filter-test="*1fg_tap*"�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�hdheuhhThh,hK�hj�  hhubhE)��}�(h��The ``--filter-device`` argument enables selective running of tests through
basic shell-style device name matching. The device names matched are the
litest-specific shortnames, see the output of ``--list``. For example:�h]�(h9�The �����}�(h�The �hj-  hhhNhNubhp)��}�(h�``--filter-device``�h]�h9�--filter-device�����}�(h�--filter-device�hj6  ubah}�(h]�h]�h]�h]�h]�uhhohj-  ubh9�� argument enables selective running of tests through
basic shell-style device name matching. The device names matched are the
litest-specific shortnames, see the output of �����}�(h�� argument enables selective running of tests through
basic shell-style device name matching. The device names matched are the
litest-specific shortnames, see the output of �hj-  hhhNhNubhp)��}�(h�
``--list``�h]�h9�--list�����}�(h�--list�hjJ  ubah}�(h]�h]�h]�h]�h]�uhhohj-  ubh9�. For example:�����}�(h�. For example:�hj-  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhU)��}�(h�=$ ./builddir/libinput-test-suite --filter-device="synaptics*"�h]�h9�=$ ./builddir/libinput-test-suite --filter-device="synaptics*"�����}�(hhhjd  ubah}�(h]�h]�h]�h]�h]�hdheuhhThh,hK�hj�  hhubhE)��}�(h��The ``--filter-group`` argument enables selective running of test groups
through basic shell-style test group matching. The test groups matched are
litest-specific test groups, see the output of ``--list``. For example:�h]�(h9�The �����}�(h�The �hjr  hhhNhNubhp)��}�(h�``--filter-group``�h]�h9�--filter-group�����}�(h�--filter-group�hj{  ubah}�(h]�h]�h]�h]�h]�uhhohjr  ubh9�� argument enables selective running of test groups
through basic shell-style test group matching. The test groups matched are
litest-specific test groups, see the output of �����}�(h�� argument enables selective running of test groups
through basic shell-style test group matching. The test groups matched are
litest-specific test groups, see the output of �hjr  hhhNhNubhp)��}�(h�
``--list``�h]�h9�--list�����}�(h�--list�hj�  ubah}�(h]�h]�h]�h]�h]�uhhohjr  ubh9�. For example:�����}�(h�. For example:�hjr  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhU)��}�(h�B$ ./builddir/libinput-test-suite --filter-group="touchpad:*hover*"�h]�h9�B$ ./builddir/libinput-test-suite --filter-group="touchpad:*hover*"�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�hdheuhhThh,hK�hj�  hhubhE)��}�(h��The ``--filter-device`` and ``--filter-group`` arguments can be combined with
``--list`` to show which groups and devices will be affected.�h]�(h9�The �����}�(h�The �hj�  hhhNhNubhp)��}�(h�``--filter-device``�h]�h9�--filter-device�����}�(h�--filter-device�hj�  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9� and �����}�(h� and �hj�  hhhNhNubhp)��}�(h�``--filter-group``�h]�h9�--filter-group�����}�(h�--filter-group�hj�  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9�  arguments can be combined with
�����}�(h�  arguments can be combined with
�hj�  hhhNhNubhp)��}�(h�
``--list``�h]�h9�--list�����}�(h�--list�hj�  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9�3 to show which groups and devices will be affected.�����}�(h�3 to show which groups and devices will be affected.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubh)��}�(h�.. _test-verbosity:�h]�h}�(h]�h]�h]�h]�h]�h*�test-verbosity�uhhhK�hj�  hhhh,ubeh}�(h]�(�selective-running-of-tests�j�  eh]�h]�(�selective running of tests��test-filtering�eh]�h]�uhh-hh/hhhh,hKLj�  }�j  j�  sj�  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Controlling test output�h]�h9�Controlling test output�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj  hhhh,hK�ubhE)��}�(h��Each test supports the ``--verbose`` commandline option to enable debugging
output, see **libinput_log_set_priority()** for details. The ``LITEST_VERBOSE``
environment variable, if set, also enables verbose mode.�h]�(h9�Each test supports the �����}�(h�Each test supports the �hj)  hhhNhNubhp)��}�(h�``--verbose``�h]�h9�	--verbose�����}�(h�	--verbose�hj2  ubah}�(h]�h]�h]�h]�h]�uhhohj)  ubh9�4 commandline option to enable debugging
output, see �����}�(h�4 commandline option to enable debugging
output, see �hj)  hhhNhNubjm  )��}�(h�**libinput_log_set_priority()**�h]�h9�libinput_log_set_priority()�����}�(h�libinput_log_set_priority()�hjF  ubah}�(h]�h]�h]�h]�h]�uhjl  hj)  ubh9� for details. The �����}�(h� for details. The �hj)  hhhNhNubhp)��}�(h�``LITEST_VERBOSE``�h]�h9�LITEST_VERBOSE�����}�(h�LITEST_VERBOSE�hjZ  ubah}�(h]�h]�h]�h]�h]�uhhohj)  ubh9�9
environment variable, if set, also enables verbose mode.�����}�(h�9
environment variable, if set, also enables verbose mode.�hj)  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  hhubhU)��}�(h�T$ ./builddir/libinput-test-suite --verbose
$ LITEST_VERBOSE=1 meson test -C builddir�h]�h9�T$ ./builddir/libinput-test-suite --verbose
$ LITEST_VERBOSE=1 meson test -C builddir�����}�(hhhjt  ubah}�(h]�h]�h]�h]�h]�hdheuhhThh,hK�hj  hhubh)��}�(h�.. _test-installed:�h]�h}�(h]�h]�h]�h]�h]�h*�test-installed�uhhhK�hj  hhhh,ubeh}�(h]�(�controlling-test-output�j  eh]�h]�(�controlling test output��test-verbosity�eh]�h]�uhh-hh/hhhh,hK�j�  }�j�  j  sj�  }�j  j  subh.)��}�(hhh]�(h3)��}�(h�Installing the test suite�h]�h9�Installing the test suite�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK�ubhE)��}�(h��If libinput is configured to install the tests, the test suite is available
as the ``libinput test-suite`` command. When run as installed binary, the
behavior of the test suite changes:�h]�(h9�SIf libinput is configured to install the tests, the test suite is available
as the �����}�(h�SIf libinput is configured to install the tests, the test suite is available
as the �hj�  hhhNhNubhp)��}�(h�``libinput test-suite``�h]�h9�libinput test-suite�����}�(h�libinput test-suite�hj�  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9�O command. When run as installed binary, the
behavior of the test suite changes:�����}�(h�O command. When run as installed binary, the
behavior of the test suite changes:�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�?the ``libinput.so`` used is the one in the library lookup paths�h]�hE)��}�(hj�  h]�(h9�the �����}�(h�the �hj�  ubhp)��}�(h�``libinput.so``�h]�h9�libinput.so�����}�(h�libinput.so�hj�  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9�, used is the one in the library lookup paths�����}�(h�, used is the one in the library lookup paths�hj�  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubj�  )��}�(h�^no system-wide quirks are installed by the test suite, only those specific
to the test devices�h]�hE)��}�(h�^no system-wide quirks are installed by the test suite, only those specific
to the test devices�h]�h9�^no system-wide quirks are installed by the test suite, only those specific
to the test devices�����}�(hj  hj  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubj�  )��}�(h�qtest device-specific quirks are installed in the system-wide quirks
directory, usually ``/usr/share/libinput/``.
�h]�hE)��}�(h�ptest device-specific quirks are installed in the system-wide quirks
directory, usually ``/usr/share/libinput/``.�h]�(h9�Wtest device-specific quirks are installed in the system-wide quirks
directory, usually �����}�(h�Wtest device-specific quirks are installed in the system-wide quirks
directory, usually �hj  ubhp)��}�(h�``/usr/share/libinput/``�h]�h9�/usr/share/libinput/�����}�(h�/usr/share/libinput/�hj$  ubah}�(h]�h]�h]�h]�h]�uhhohj  ubh9�.�����}�(h�.�hj  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhj�  hh,hK�hj�  hhubhE)��}�(h��It is not advisable to run ``libinput test-suite`` on a production machine.
Data loss may occur. The primary use-case for the installed test suite is
verification of distribution composes.�h]�(h9�It is not advisable to run �����}�(h�It is not advisable to run �hjL  hhhNhNubhp)��}�(h�``libinput test-suite``�h]�h9�libinput test-suite�����}�(h�libinput test-suite�hjU  ubah}�(h]�h]�h]�h]�h]�uhhohjL  ubh9�� on a production machine.
Data loss may occur. The primary use-case for the installed test suite is
verification of distribution composes.�����}�(h�� on a production machine.
Data loss may occur. The primary use-case for the installed test suite is
verification of distribution composes.�hjL  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubh�)��}�(h��The ``prefix`` is still used by the test suite. For verification
of a system package, the test suite must be configured with the same prefix.�h]�hE)��}�(h��The ``prefix`` is still used by the test suite. For verification
of a system package, the test suite must be configured with the same prefix.�h]�(h9�The �����}�(h�The �hjs  ubhp)��}�(h�
``prefix``�h]�h9�prefix�����}�(h�prefix�hj|  ubah}�(h]�h]�h]�h]�h]�uhhohjs  ubh9� is still used by the test suite. For verification
of a system package, the test suite must be configured with the same prefix.�����}�(h� is still used by the test suite. For verification
of a system package, the test suite must be configured with the same prefix.�hjs  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjo  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  hhhh,hNubhE)��}�(h�[To configure libinput to install the tests, use the ``-Dinstall-tests=true``
meson option::�h]�(h9�4To configure libinput to install the tests, use the �����}�(h�4To configure libinput to install the tests, use the �hj�  hhhNhNubhp)��}�(h�``-Dinstall-tests=true``�h]�h9�-Dinstall-tests=true�����}�(h�-Dinstall-tests=true�hj�  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9�
meson option:�����}�(h�
meson option:�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhU)��}�(h�B$ meson builddir -Dtests=true -Dinstall-tests=true <other options>�h]�h9�B$ meson builddir -Dtests=true -Dinstall-tests=true <other options>�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�hdheuhhThh,hK�hj�  hhubh)��}�(h�.. _test-meson-suites:�h]�h}�(h]�h]�h]�h]�h]�h*�test-meson-suites�uhhhK�hj�  hhhh,ubeh}�(h]�(�installing-the-test-suite�j�  eh]�h]�(�installing the test suite��test-installed�eh]�h]�uhh-hh/hhhh,hK�j�  }�j�  j�  sj�  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Meson test suites�h]�h9�Meson test suites�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK�ubhE)��}�(h�sThis section is primarily of interest to distributors that want to run test
or developers working on libinput's CI.�h]�h9�uThis section is primarily of interest to distributors that want to run test
or developers working on libinput’s CI.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhE)��}�(h��Tests invoked by ``meson test`` are grouped into test suites, the test suite
names identify when the respective test can be run:�h]�(h9�Tests invoked by �����}�(h�Tests invoked by �hj  hhhNhNubhp)��}�(h�``meson test``�h]�h9�
meson test�����}�(h�
meson test�hj  ubah}�(h]�h]�h]�h]�h]�uhhohj  ubh9�a are grouped into test suites, the test suite
names identify when the respective test can be run:�����}�(h�a are grouped into test suites, the test suite
names identify when the respective test can be run:�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubj�  )��}�(hhh]�(j�  )��}�(h�P``valgrind``: tests that can be run under valgrind (in addition to a
normal run)�h]�hE)��}�(h�P``valgrind``: tests that can be run under valgrind (in addition to a
normal run)�h]�(hp)��}�(h�``valgrind``�h]�h9�valgrind�����}�(h�valgrind�hj0  ubah}�(h]�h]�h]�h]�h]�uhhohj,  ubh9�D: tests that can be run under valgrind (in addition to a
normal run)�����}�(h�D: tests that can be run under valgrind (in addition to a
normal run)�hj,  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj(  ubah}�(h]�h]�h]�h]�h]�uhj�  hj%  hhhh,hNubj�  )��}�(h�(``root``: tests that must be run as root�h]�hE)��}�(hjR  h]�(hp)��}�(h�``root``�h]�h9�root�����}�(h�root�hjW  ubah}�(h]�h]�h]�h]�h]�uhhohjT  ubh9� : tests that must be run as root�����}�(h� : tests that must be run as root�hjT  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjP  ubah}�(h]�h]�h]�h]�h]�uhj�  hj%  hhhh,hNubj�  )��}�(h�9``hardware``: tests that require a VM or physical machine�h]�hE)��}�(hjy  h]�(hp)��}�(h�``hardware``�h]�h9�hardware�����}�(h�hardware�hj~  ubah}�(h]�h]�h]�h]�h]�uhhohj{  ubh9�-: tests that require a VM or physical machine�����}�(h�-: tests that require a VM or physical machine�hj{  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjw  ubah}�(h]�h]�h]�h]�h]�uhj�  hj%  hhhh,hNubj�  )��}�(h�o``all``: all tests, only needed because of
`meson bug 5340 <https://github.com/mesonbuild/meson/issues/5340>`_
�h]�hE)��}�(h�n``all``: all tests, only needed because of
`meson bug 5340 <https://github.com/mesonbuild/meson/issues/5340>`_�h]�(hp)��}�(h�``all``�h]�h9�all�����}�(h�all�hj�  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9�$: all tests, only needed because of
�����}�(h�$: all tests, only needed because of
�hj�  ubh �	reference���)��}�(h�C`meson bug 5340 <https://github.com/mesonbuild/meson/issues/5340>`_�h]�h9�meson bug 5340�����}�(h�meson bug 5340�hj�  ubah}�(h]�h]�h]�h]�h]��name��meson bug 5340��refuri��/https://github.com/mesonbuild/meson/issues/5340�uhj�  hj�  ubh)��}�(h�2 <https://github.com/mesonbuild/meson/issues/5340>�h]�h}�(h]��meson-bug-5340�ah]�h]��meson bug 5340�ah]�h]��refuri�j�  uhh�
referenced�Khj�  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj%  hhhh,hNubeh}�(h]�h]�h]�h]�h]�jJ  jK  uhj�  hh,hK�hj�  hhubhE)��}�(h��The suite names can be provided as filters to ``meson test
--suite=<suitename>`` or ``meson test --no-suite=<suitename>``.
For example, if running a container-based CI, you may specify the test
suites as:�h]�(h9�.The suite names can be provided as filters to �����}�(h�.The suite names can be provided as filters to �hj�  hhhNhNubhp)��}�(h�"``meson test
--suite=<suitename>``�h]�h9�meson test
--suite=<suitename>�����}�(h�meson test
--suite=<suitename>�hj�  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9� or �����}�(h� or �hj�  hhhNhNubhp)��}�(h�%``meson test --no-suite=<suitename>``�h]�h9�!meson test --no-suite=<suitename>�����}�(h�!meson test --no-suite=<suitename>�hj  ubah}�(h]�h]�h]�h]�h]�uhhohj�  ubh9�S.
For example, if running a container-based CI, you may specify the test
suites as:�����}�(h�S.
For example, if running a container-based CI, you may specify the test
suites as:�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhU)��}�(h��$ meson test --no-suite=machine  # only run container-friendly tests
$ meson test --suite=valgrind --setup=valgrind  # run all valgrind-compatible tests
$ meson test --no-suite=root  # run all tests not requiring root�h]�h9��$ meson test --no-suite=machine  # only run container-friendly tests
$ meson test --suite=valgrind --setup=valgrind  # run all valgrind-compatible tests
$ meson test --no-suite=root  # run all tests not requiring root�����}�(hhhj&  ubah}�(h]�h]�h]�h]�h]�hdheuhhThh,hK�hj�  hhubhE)��}�(h�/These suites are subject to change at any time.�h]�h9�/These suites are subject to change at any time.�����}�(hj6  hj4  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubeh}�(h]�(�meson-test-suites�j�  eh]�h]�(�meson test suites��test-meson-suites�eh]�h]�uhh-hh/hhhh,hK�j�  }�jH  j�  sj�  }�j�  j�  subeh}�(h]�(�libinput-test-suite�h+eh]�h]�(�libinput test suite��
test-suite�eh]�h]�uhh-hhhhhh,hKj�  }�jS  h sj�  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j{  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`f65b1a2`�h]�j�  )��}�(h�git commit f65b1a2�h]�h9�git commit f65b1a2�����}�(h�f65b1a2�hj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/f65b1a2�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  h�<rst_prolog>�hKhhub�git_version_full�j�  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f38617dc830>`

�h]�j�  )��}�(h�<git commit <function get_git_version_full at 0x7f38617dc830>�h]�h9�<git commit <function get_git_version_full at 0x7f38617dc830>�����}�(h�1<function get_git_version_full at 0x7f38617dc830>�hj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f38617dc830>�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  hj�  hKhhubu�substitution_names�}�(�git_version�j�  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h ajK  ]�jA  aj�  ]�j�  aj2  ]�j(  aj�  ]�j�  aj  ]�j  aj�  ]�j�  aj�  ]�j�  au�nameids�}�(jS  h+jR  jO  j�  jK  j�  j�  j9  j�  j8  j5  j�  j2  j�  j�  j  j�  j  j  j�  j  j�  j�  j�  j�  j�  j�  jH  j�  jG  jD  j�  j�  u�	nametypes�}�(jS  �jR  Nj�  �j�  Nj9  �j8  Nj�  �j�  Nj  �j  Nj�  �j�  Nj�  �j�  NjH  �jG  Nj�  �uh}�(h+h/jO  h/jK  jL  j�  jL  j�  j�  j5  j�  j2  j>  j�  j>  j�  j�  j  j�  j  j  j�  j  j�  j�  j�  j�  j�  j�  jD  j�  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�0Hyperlink target "test-suite" is not referenced.�����}�(hhhj   ubah}�(h]�h]�h]�h]�h]�uhhDhj  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�Kuhj  ubj  )��}�(hhh]�hE)��}�(hhh]�h9�6Hyperlink target "test-job-control" is not referenced.�����}�(hhhj;  ubah}�(h]�h]�h]�h]�h]�uhhDhj8  ubah}�(h]�h]�h]�h]�h]��level�K�type�j5  �source�h,�line�K&uhj  ubj  )��}�(hhh]�hE)��}�(hhh]�h9�1Hyperlink target "test-config" is not referenced.�����}�(hhhjU  ubah}�(h]�h]�h]�h]�h]�uhhDhjR  ubah}�(h]�h]�h]�h]�h]��level�K�type�j5  �source�h,�line�K3uhj  ubj  )��}�(hhh]�hE)��}�(hhh]�h9�/Hyperlink target "test-root" is not referenced.�����}�(hhhjo  ubah}�(h]�h]�h]�h]�h]�uhhDhjl  ubah}�(h]�h]�h]�h]�h]��level�K�type�j5  �source�h,�line�KAuhj  ubj  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "test-filtering" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j5  �source�h,�line�KMuhj  ubj  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "test-verbosity" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j5  �source�h,�line�K�uhj  ubj  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "test-installed" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j5  �source�h,�line�K�uhj  ubj  )��}�(hhh]�hE)��}�(hhh]�h9�7Hyperlink target "test-meson-suites" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j5  �source�h,�line�K�uhj  ube�transformer�N�
decoration�Nhhub.